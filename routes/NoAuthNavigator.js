import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import AnimatedScreen from "../screens/AnimatedScreen";
import ProductHome from "../screens/product-home/ProductHome";
import MyAds from "../screens/my-ads/MyAds";
import Store from "../screens/store/Store";
import Conversation from "../screens/conversation/Conversation";
import Chat from "../screens/chats/Chat";
import ProductDetails from "../screens/product-details/ProductDetails";
import AddPost from "../screens/add-post/AddPost";
import Profile from "../screens/profile/Profile";
import Video from "../screens/video/Video";
import NearAds from "../screens/Near-Ads/NearAds";
import Settings from "../screens/Setting/Settings";
import BuyCredit from "../screens/Buy-Credit/BuyCredit";
import Notification from "../screens/Notifications/Notification";
import Search from "../screens/search/Search";
import Categories from "../screens/categories/Categories";
import LoginScreen from "../screens/login-screen/LoginScreen";
import PhoneVerification from "../screens/phone-Verification/PhoneVerification";
import SubCategory from "../screens/sub-category/SubCategory";

const { Navigator, Screen } = createStackNavigator();

function NoAuthNavigation() {
  return (
    <Navigator headerMode="none">
      <Screen name="LoginScreen" component={LoginScreen} />
      <Screen name="PhoneVerification" component={PhoneVerification} />
      <Screen name="ProductHome" component={ProductHome} />
      <Screen name="MyAds" component={MyAds} />
      <Screen name="Store" component={Store} />
      <Screen name="Chat" component={Chat} />
      <Screen name="Conversation" component={Conversation} />
      <Screen name="ProductDetails" component={ProductDetails} />
      <Screen name="AddPost" component={AddPost} />
      <Screen name="Profile" component={Profile} />
      <Screen name="Video" component={Video} />
      <Screen name="NearAds" component={NearAds} />
      <Screen name="Settings" component={Settings} />
      <Screen name="BuyCredit" component={BuyCredit} />
      <Screen name="Notification" component={Notification} />
      <Screen name="Search" component={Search} />
      <Screen name="Categories" component={Categories} />
      <Screen name="SubCategory" component={SubCategory} />
    </Navigator>
  );
}
export const NoAuthNavigator = () => (
  <NavigationContainer>
    <NoAuthNavigation />
  </NavigationContainer>
);
