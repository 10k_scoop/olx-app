import {
  GET_LANG,
  SET_LANG,
  DO_LOGIN,
  DO_LOGOUT,
  GET_USER_DATA,
  SET_USER_DATA,
  GET_CATEGORIES,
  ADD_POST,
  GET_PRODUCTS,
  GET_SUBCATEGORY,
} from "../types/types";
const initialState = {
  set_Lang: null,
  get_Lang: null,
  login_details: null,
  logout: null,
  set_user_data: null,
  get_user_data: null,
  get_products: null,
  get_categories: null,
  add_post: null,
  get_subcategory:null
};
const mainReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LANG:
      return {
        ...state,
        set_Lang: action.payload,
      };
    case GET_LANG:
      return {
        ...state,
        get_Lang: action.payload,
      };
    case DO_LOGIN:
      return {
        ...state,
        login_details: action.payload,
      };
    case DO_LOGOUT:
      return {
        ...state,
        logout: action.payload,
      };

    case GET_USER_DATA:
      return {
        ...state,
        get_user_data: action.payload,
      };
    case SET_USER_DATA:
      return {
        ...state,
        set_user_data: action.payload,
      };
    case GET_CATEGORIES:
      return {
        ...state,
        get_categories: action.payload,
      };
    case GET_PRODUCTS:
      return {
        ...state,
        get_products: action.payload,
      };
    case ADD_POST:
      return {
        ...state,
        add_post: action.payload,
      };
    case GET_SUBCATEGORY:
      return {
        ...state,
        get_subcategory: action.payload,
      };
    default:
      return state;
  }
};
export default mainReducer;
