import { DO_LOGIN, GET_ERRORS,DO_LOGOUT } from "../../types/types";
import * as Facebook from "expo-facebook";
import * as firebase from "firebase";
export const socialLoginFacebook = (val) => async (dispatch) => {
  try {
    await Facebook.initializeAsync({
      appId: "2405982743029036",
      appName: "Herbew",
    });

    const { type, token } = await Facebook.logInWithReadPermissionsAsync({
      permissions: ["public_profile","email"],
    });

    if (type === "success") {
      // Build Firebase credential with the Facebook access token.
      const credential = firebase.auth.FacebookAuthProvider.credential(token);
      // Sign in with credential from the Facebook user.
      firebase
        .auth()
        .signInWithCredential(credential)
        .then((res) => {
          dispatch({ type: DO_LOGIN, payload: res.user });
        })
        .catch((error) => {
          // Handle Errors here.
          console.log("success error");
          console.log(error);
        });
    }
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
    console.log(e);
  }
};

export const logout = () => async (dispatch) => {
  try {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
      dispatch({ type: DO_LOGOUT, payload: "Logged Out" });
    }).catch((error) => {
      // An error happened.
    });    
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
  }
};
export const BackAction = (props) => {
  //user ? BackHandler.exitApp() : props.navigation.navigate("LoginScreen");
  alert()
};

