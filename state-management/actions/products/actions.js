import {
  GET_CATEGORIES,
  GET_PRODUCTS,
  ADD_POST,
  GET_SUBCATEGORY,
} from "../../types/types";
import * as firebase from "firebase";
export const getCategories = (db, setLoading) => async (dispatch) => {
  db.collection("categories").onSnapshot((querySnapshot) => {
    var data = [];
    querySnapshot.forEach((documentSnapshot) => {
      data.push({
        ...documentSnapshot.data(),
        key: documentSnapshot.id,
      });
    });
    dispatch({ type: GET_CATEGORIES, payload: data });
    setLoading(false);
  });
};

export const getProducts = (db, setLoading) => async (dispatch) => {
  db.collection("products").onSnapshot((querySnapshot) => {
    var data = [];
    querySnapshot.forEach((documentSnapshot) => {
      data.push({
        ...documentSnapshot.data(),
        key: documentSnapshot.id,
      });
    });
    dispatch({ type: GET_PRODUCTS, payload: data });
    setLoading(false);
  });
};

export const getSubCategory =
  (db, setLoading, title, setCategories) => async (dispatch) => {
    db.collection("categories")
      .where("name", "==", title)
      .get()
      .then((querySnapshot) => {
        var data = [];
        querySnapshot.forEach((documentSnapshot) => {
          data.push({
            ...documentSnapshot.data(),
            key: documentSnapshot.id,
          });
        });
        dispatch({ type: GET_SUBCATEGORY, payload: data });
        setCategories(JSON.parse(data[0].subCategory));
        setLoading(false);
      });
  };

export const addPost = (db, setLoading, data) => async (dispatch) => {
  const randomId=Math.floor(Math.random() * 67677836647646)
  const randomId2=Math.floor(Math.random() * 52525836647646)
  const response = await fetch(data.uri);
  const blob = await response.blob();
  var ref = firebase.storage().ref().child(randomId+data.addOwner+(randomId+randomId2));
  const snapshot = await ref.put(blob);
  snapshot.ref
    .getDownloadURL()
    .then((res) => {
      db.collection("products")
        .add({
          addOwner: data.addOwner,
          category: data.category,
          currency: "$",
          image: res,
          location: data.location,
          peopleReached: 0,
          price: data.price,
          status: true,
          subCategory: data.subCategory,
          title: data.title,
          desc: data.desc,
        })
        .then(async (docRef) => {
          alert("Your advertisement is live now");
          setLoading(false);
        })
        .catch((error) => {
          console.error("Error adding document: ", error);
        });
    })
    .catch((err) => {
      console.log(err);
    });
};
