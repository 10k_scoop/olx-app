import { GET_LANG, SET_LANG, GET_ERRORS } from "../../types/types";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const setLang = (val,setLanguage) => async (dispatch) => {
  var newLang = val == "en" ? "ar" : "en";
  try {
    await AsyncStorage.setItem("lang", newLang);
    setLanguage(newLang)
    dispatch({ type: SET_LANG, payload: newLang });
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
  }
};

export const getLang = (setLanguage) => async (dispatch) => {
  try {
    const value = await AsyncStorage.getItem("lang");
    if (value !== null) {
      dispatch({ type: GET_LANG, payload: value });
      setLanguage(value)
    } else {
      dispatch({ type: GET_LANG, payload: "en" });
      setLanguage("en")
    }
  } catch (e) {
    dispatch({ type: GET_ERRORS, payload: e });
  }
};
