import React, { useState, useRef, useEffect } from "react";
import {
  ImageBackground,
  StyleSheet,
  View,
  SafeAreaView,
  StatusBar,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import FilterBtn from "../../components/FilterBtn";
import CategoryCard from "./components/CategoryCard";
import Header1 from "../../components/Header1";
import BottomMenu from "../../components/BottomMenu";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { getCategories } from "../../state-management/actions/products/actions";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as firebase from "firebase";
import AnimatedScreen from "../AnimatedScreen";
import * as Animatable from "react-native-animatable";

const ProductHome = (props) => {
  const [lang, setLanguage] = useState();
  const anime = useRef(null);
  const [showAnime, setShowAnime] = useState(false);
  const user = firebase.auth().currentUser;
  const [loading, setLoading] = useState(true);
  const db = firebase.firestore();
  const [categories, setCategories] = useState();

  useEffect(() => {
    animation();
    props?.getLang(setLanguage);
    props?.getCategories(db, setLoading);
    user ? setUserData() : undefined;
  }, []);

  useEffect(() => {
    setCategories(props.get_categories);
  }, [props]);

  const animation = async () => {
    // setShowAnime(true);
    // setTimeout(() => {
    //   setShowAnime(false);
    //   console.log("yeah");
    // }, 2700);
  };

  const setUserData = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      if (value !== null) {
        // We have data!!
        console.log(value);
      } else {
        await AsyncStorage.setItem(
          "userData",
          JSON.stringify(props?.login_details.providerData[0])
        );
      }
    } catch (err) {}
  };

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <Header1
        screen="Home"
        onBellPress={() => props.navigation.navigate("Notification")}
        onSearchPress={() => props.navigation.navigate("Search")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.MenuBar}>
          <FilterBtn
            txt={Strings("Myads", lang)}
            onPress={() => props.navigation.navigate("MyAds")}
          />
          <FilterBtn
            txt={Strings("Nearads", lang)}
            onPress={() => props.navigation.navigate("NearAds")}
          />
          <FilterBtn
            txt={Strings("Store", lang)}
            onPress={() => props.navigation.navigate("Store")}
          />
        </View>

        <View style={styles.container1}>
          <ImageBackground
            style={{ width: "100%", height: "100%" }}
            source={require("../../assets/AdPic.jpeg")}
            resizeMode="cover"
          ></ImageBackground>
        </View>

        <View style={styles.container2}>
          {categories.map((item, index) => {
            return (
              <CategoryCard
                img={require("../../assets/CategoryPic.png")}
                // txt={Strings("mobile", lang) + " " + index}
                txt={item.name}
                key={index}
                onPress={() =>
                  props.navigation.navigate("SubCategory", {
                    title: item.name,
                    data: JSON.parse(item.subCategory),
                  })
                }
              />
            );
          })}
        </View>
      </ScrollView>
      {!showAnime && (
        <BottomMenu
          active="home"
          onChatPress={() => props.navigation.navigate("Chat")}
          onAddPostPress={() =>
            user
              ? props.navigation.navigate("AddPost")
              : alert("You must login to post a add")
          }
          onProfilePress={() =>
            user
              ? props.navigation.navigate("Profile")
              : alert("You must login first !")
          }
          onVideoPress={() => props.navigation.navigate("Video")}
          keywords={{
            home: Strings("Category", lang),
            chat: Strings("Chat", lang),
            video: Strings("Video", lang),
            addPost: Strings("addPost", lang),
            profile: Strings("Profile", lang),
          }}
        />
      )}
      {showAnime && (
        <Animatable.View ref={anime} style={styles.animeScreen}>
          <View style={styles.animeScreen}>
            <AnimatedScreen />
          </View>
        </Animatable.View>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    alignItems: "center",
    paddingHorizontal: 10,
  },
  MenuBar: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-around",
    marginBottom: 10,
  },
  container1: {
    width: wp("95%"),
    height: hp("22%"),
    borderRadius: 10,
    overflow: "hidden",
    marginBottom: 10,
  },
  animeScreen: {
    position: "absolute",
    width: wp("100%"),
    height: hp("100%") + StatusBar.currentHeight,
    backgroundColor: "#2ECC71",
    zIndex: 9999999999999999,
  },
  container2: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "flex-start",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
  login_details: state.main.login_details,
  get_categories: state.main.get_categories,
});
export default connect(mapStateToProps, { getLang, getCategories })(
  ProductHome
);
