import React from "react";
import { StyleSheet, TouchableOpacity, View, Text, Image } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function CategoryCard({ txt, img,onPress }) {
  return (
    <TouchableOpacity style={styles.cont1} onPress={onPress}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.Inner}>
          <View style={styles.Category}>
            <Text style={{ fontSize: rf(16),fontWeight:'700' }}>{txt}</Text>
          </View>
          <View style={styles.CategoryPic}>
            <View style={styles.CategoryPicInner}>
              <Image
                style={{ width: "90%", height: "80%" }}
                resizeMode="contain"
                source={img}
              />
            </View>
          </View>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("45%"),
    height: hp("17%"),
    borderRadius: 15,
    overflow: "hidden",
    marginBottom:5,
    marginHorizontal:2
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  Inner: {
    width: wp("45%"),
    height: hp("17%"),
  },
  Category: {
    width: "100%",
    height: "20%",
    justifyContent: "flex-end",
    paddingHorizontal: 15,
  },
  CategoryPic: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  CategoryPicInner: {
    width: "60%",
    height: "100%",
  },
  ForwardArrow: {
    width: wp('15%'),
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  ForwardArrowInner: {
    width: wp('8%'),
    height: hp('4%'),
    borderRadius: 10,
    justifyContent: "center",
    backgroundColor: "#2ECC71",
    alignItems: "center",
  },
});
