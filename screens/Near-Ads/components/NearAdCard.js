import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  AntDesign,
  Ionicons,
  MaterialCommunityIcons,
} from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Strings from "../../../constants/lng/LocalizationStrings";
import { getLang } from "../../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const NearAdCard = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <TouchableOpacity style={styles.cont1} onPress={props.onPress}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.Inner}>
          <View style={styles.img}>
            <ImageBackground
              style={{ width: "100%", height: "100%", borderRadius: 10 }}
              source={require("../../../assets/NearAds.jpg")}
              resizeMode="stretch"
            ></ImageBackground>
            <View style={styles.Ads}>
              <View style={styles.InnerAds}>
                <LinearGradient
                  // Background Linear Gradient
                  colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                  start={[0.0, 0.5]}
                  end={[1.0, 0.5]}
                  locations={[0.2, 1, 1]}
                  style={styles.background}
                />
                <Text
                  style={{
                    fontSize: rf(11),
                    color: "#2ECC71",
                    fontWeight: "600",
                  }}
                >
                  {Strings("ads", lang)}
                </Text>
              </View>
            </View>
            <View style={styles.Icon}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <AntDesign name="heart" size={rf(15)} color="#2ECC71" />
            </View>
          </View>
          <View style={styles.TitleDetails}>
            <View style={styles.FirstRow}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <Text style={{ fontSize: rf(18), fontWeight: "600" }}>
                {Strings("Title", lang)}
              </Text>
            </View>

            <View style={styles.SecondRow}>
              <View style={styles.InnerSecondRow}>
                <LinearGradient
                  // Background Linear Gradient
                  colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                  start={[0.0, 0.5]}
                  end={[1.0, 0.5]}
                  locations={[0.2, 1, 1]}
                  style={styles.background}
                />
                <Ionicons name="eye" size={rf(13)} color="#2ECC71" style={{paddingLeft:5}} />
                <Text style={{paddingRight:5, fontSize: rf(10), marginLeft: 2 }}>
                  1 {Strings("min", lang)}
                </Text>
              </View>
              <View style={styles.InnerSecondRow}>
                <LinearGradient
                  // Background Linear Gradient
                  colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                  start={[0.0, 0.5]}
                  end={[1.0, 0.5]}
                  locations={[0.2, 1, 1]}
                  style={styles.background}
                />
                <Ionicons name="ios-location" size={rf(13)} color="#2ECC71" style={{paddingLeft:5}} />
                <Text style={{paddingRight:5, fontSize: rf(10), marginLeft: 2 }}>
                  2 {Strings("kilometer", lang)}
                </Text>
              </View>
            </View>
            <View style={styles.LastRow}>
              <View style={styles.InnerLastRow}>
                <LinearGradient
                  // Background Linear Gradient
                  colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                  start={[0.0, 0.5]}
                  end={[1.0, 0.5]}
                  locations={[0.2, 1, 1]}
                  style={styles.background}
                />
                <MaterialCommunityIcons
                  name="human-greeting"
                  size={rf(13)}
                  color="#2ECC71"
                />
                <Text style={{ fontSize: rf(10), marginLeft: 2 }}>
                  {Strings("username", lang)}
                </Text>
              </View>
              <View style={styles.InnerLastRow}>
                <LinearGradient
                  // Background Linear Gradient
                  colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                  start={[0.0, 0.5]}
                  end={[1.0, 0.5]}
                  locations={[0.2, 1, 1]}
                  style={styles.background}
                />
                <Text style={{ fontSize: rf(10) }}>{Strings("Riyad", lang)}</Text>
              </View>
            </View>
          </View>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cont1: {
    width: wp("96%"),
    height: hp("27%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginHorizontal: wp("2%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  Inner: {
    textAlign: "center",
    fontSize: rf(13),
    width: wp("96%"),
    height: hp("27%"),
    textAlignVertical: "center",
    overflow: "hidden",
    flexDirection: "row",
  },
  img: {
    width: "40%",
    height: "100%",
    borderRadius: 10,
    overflow: "hidden",
  },
  Ads: {
    width: "100%",
    height: "10%",
    position: "absolute",
    alignItems: "flex-end",
  },
  InnerAds: {
    width: "35%",
    height: "100%",
    borderRadius: 5,
    alignItems: "center",
    borderColor: "#fff",
    borderWidth: 1,
    justifyContent: "center",
  },
  Icon: {
    width: hp("4%"),
    height: hp("4%"),
    borderRadius: 100,
    overflow: "hidden",
    position: "absolute",
    left: 10,
    top: 20,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 10,
  },
  TitleDetails: {
    width: "60%",
    height: "100%",
    alignItems: "center",
  },
  FirstRow: {
    width: "90%",
    height: "25%",
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: "20%",
    backgroundColor: "#fff",
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 5,
  },
  SecondRow: {
    width: "90%",
    height: "23%",
    justifyContent: "space-between",
    alignItems: "flex-end",
  },
  InnerSecondRow: {
    minWidth: "38%",
    height: "42%",
    borderRadius: 5,
    overflow: "hidden",
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 5,
  },
  LastRow: {
    width: "90%",
    height: "13%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  InnerLastRow: {
    width: "37%",
    height: "65%",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    flexDirection: "row",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,

    elevation: 5,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(NearAdCard);
