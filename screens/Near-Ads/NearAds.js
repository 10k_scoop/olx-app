import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ScrollView,
  View,
} from "react-native";
import Header1 from "../../components/Header1";
import FilterBtn from "../../components/FilterBtn";
import NearAdCard from "./components/NearAdCard";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const NearAds = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Header1
        screen="Store"
        searchPlaceholder={Strings("searchStore",lang)}
        onBackPress={() => props.navigation.goBack()}
        onSearchPress={() => props.navigation.navigate("Search")}
        onBellPress={() => props.navigation.navigate("Notification")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Menu}>
          <FilterBtn txt={Strings("Nearads",lang)} textBold textLarge />
          <FilterBtn txt={Strings("Filter",lang)} filter />
        </View>

        <NearAdCard onPress={() => props.navigation.navigate("ProductDetails")} />
        <NearAdCard />
        <NearAdCard />
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    alignItems: "center",
    paddingBottom: 10,
  },
  Menu: {
    width: wp("100%"),
    height: hp("8%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: "7%",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(NearAds);
