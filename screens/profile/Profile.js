import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import BottomMenu from "../../components/BottomMenu";
import Header1 from "../../components/Header1";
import Button from "./components/Button";
import Icon from "./components/Icon";
import List from "./components/List";
import UserDisplay from "./components/UserDisplay";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";
import { logout } from "../../state-management/actions/auth/authActions";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as firebase from "firebase";

const Profile = (props) => {
  const [lang, setLanguage] = useState();
  const [userData, setUserData] = useState();
  const [loading, setLoading] = useState();
  const user = firebase.auth().currentUser;
  useEffect(() => {
    props?.getLang(setLanguage);
    user ? getUserData() : undefined;
  }, []);

  const getUserData = async () => {
    try {
      const value = await AsyncStorage.getItem("userData");
      if (value !== null) {
        // We have data!!
        setUserData(JSON.parse(value));
        setLoading(false);
      }
    } catch (err) {}
  };

  const onLogout = async () => {
    try {
      await AsyncStorage.setItem("userData", null);
    } catch (e) {}
    props.logout()
  };

  if (loading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <Header1
        screen={"profile"}
        onBackPress={() => props.navigation.goBack()}
        onSettingPress={() => props.navigation.navigate("Settings")}
        onSearchPress={() => props.navigation.navigate("Search")}
        searchPlaceholder={Strings("searchM", lang)}
      />
      <ScrollView>
        <UserDisplay
          text={{
            name: userData?.displayName || Strings("Name", lang),
            username: userData?.email || Strings("username", lang),
            followers: Strings("Followers", lang),
            ads: Strings("ads", lang),
            photo: userData?.photoURL,
          }}
        />
        <View style={styles.accounts}>
          <Text style={styles.txt}>{Strings("Verified", lang)}</Text>
          <Icon iconName={"facebook-f"} />
          <Icon iconName={"google"} />
          <Icon iconName={"snapchat-ghost"} />
        </View>
        <View style={styles.list}>
          <InsetShadow>
            <LinearGradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.0, 0.5]}
              end={[1.0, 0.5]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <View style={[styles.inner, styles.shadow]}>
              <List text={Strings("Myads", lang)} icon="Ad" />
              <List text={Strings("myVideoAds", lang)} icon="video" />
              <List text={Strings("myOrder", lang)} icon="bag" />
              <List text={Strings("removePromotionalAds", lang)} icon="rm-ad" />
              <List text={Strings("recentViewAds", lang)} icon="recent" />
              <List
                text={Strings("myWallet", lang)}
                icon="wallet"
                onPress={() => props.navigation.navigate("BuyCredit")}
              />
              <List text={Strings("showMembership", lang)} icon="shop" />
              <List text={Strings("favoriteAds", lang)} icon="fav" />
              <List text={Strings("inviteYourFriend", lang)} icon="share" />
            </View>
          </InsetShadow>
        </View>
        <View style={styles.butt}>
          <Button txt={Strings("Logout", lang)} onPress={()=>onLogout()} />
          <Button txt={Strings("contactUs", lang)} />
        </View>
      </ScrollView>
      <BottomMenu
        active="profile"
        onHomePress={() => props.navigation.navigate("ProductHome")}
        onChatPress={() => props.navigation.navigate("Chat")}
        onAddPostPress={() => props.navigation.navigate("AddPost")}
        onVideoPress={() => props.navigation.navigate("Video")}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
  accounts: {
    width: wp("100%"),
    height: hp("8%"),
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  txt: {
    fontWeight: "bold",
    fontSize: rf(14),
  },
  list: {
    width: wp("96%"),
    height: hp("78.5%"),
    alignSelf: "center",
    borderRadius: 15,
    overflow: "hidden",
    marginBottom: 10,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
  },
  butt: {
    width: wp("90%"),
    height: hp("7%"),
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
  logout: state.main.logout,
  login_details: state.main.login_details,
});
export default connect(mapStateToProps, { getLang, logout })(Profile);
