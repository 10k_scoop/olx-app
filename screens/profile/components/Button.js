import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function Button({ txt,onPress }) {
  return (
    <TouchableOpacity style={styles.cont1} onPress={onPress}>
      <InsetShadow>
        <View style={styles.Inner}>
          <Text style={styles.txt}>{txt}</Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    height: hp("5%"),
    width: wp("30%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#fff"
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  Inner: {
    flex: 1,
    height: hp("5%"),
    width: wp("30%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#2ECC71"
  },
  txt: {
    fontSize: rf(14),
    color:"#fff",
    fontWeight:"bold"
  }
});
