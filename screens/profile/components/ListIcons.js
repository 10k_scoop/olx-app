import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const ListIcons = ({icon}) => {
    return (
      <View>
        {icon == "Ad" && (
          <View style={styles.container}>
            <Text style={styles.txt}>AD</Text>
          </View>
        )}
        {icon == "rm-ad" && (
          <View style={styles.container}>
            <View style={styles.cut}></View>
            <Text style={styles.txt}>AD</Text>
          </View>
        )}
        {icon == "recent" && (
          <View style={styles.cont2}>
            <Text style={styles.txt2}>ADS</Text>
          </View>
        )}
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    width: wp("9%"),
    height: wp("9%"),
    backgroundColor: "#2ECC71",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center"
  },
  txt: {
    fontWeight: "bold",
    color: "#fff",
    fontSize: rf(16)
  },
  cont2: {
    width: wp("9%"),
    height: wp("9%"),
    borderRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderColor: "#2ECC71"
  },
  txt2: {
    fontWeight: "bold",
    color: "#2ECC71",
    fontSize: rf(12)
  },
  cut: {
    height: wp("9%"),
    backgroundColor: "red",
    width: wp("0.5%"),
    position: "absolute",
    transform: [{ rotate: "35deg" }]
  }
});

export default ListIcons;
