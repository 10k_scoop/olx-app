import { AntDesign, Feather, FontAwesome, FontAwesome5, MaterialIcons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import ListIcons from './ListIcons';

const List = ({text, icon,onPress}) => {
    return (
      <TouchableOpacity style={[styles.container, styles.shadow]} onPress={onPress}>
        <InsetShadow>
          <LinearGradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.inner}>
            {icon == "video" && (
              <MaterialIcons
                name="ondemand-video"
                size={rf(30)}
                color="#2ECC71"
              />
            )}
            {icon == "bag" && (
              <Feather name="shopping-bag" size={rf(30)} color="#2ECC71" />
            )}
            {icon == "wallet" && (
              <FontAwesome5 name="wallet" size={rf(30)} color="#2ECC71" />
            )}
            {icon == "shop" && (
              <FontAwesome5 name="shopify" size={rf(30)} color="#2ECC71" />
            )}
            {icon == "fav" && (
              <AntDesign name="hearto" size={rf(30)} color="#2ECC71" />
            )}
            {icon == "share" && (
              <FontAwesome name="share" size={rf(30)} color="#2ECC71" />
            )}
            {icon == "Ad" && <ListIcons icon={icon} />}
            {icon == "rm-ad" && <ListIcons icon={icon} />}
            {icon == "recent" && <ListIcons icon={icon} />}
            <Text style={styles.txt}>{text}</Text>
          </View>
        </InsetShadow>
      </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
  container: {
    width: "92%",
    height: hp("7%"),
    marginTop: hp('1.5%'),
    borderRadius: 15,
    overflow:"hidden"
  },
  inner: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    alignItems:"center",
    paddingHorizontal: wp('5%')
  },
  txt:{
    fontSize: rf(14),
    paddingLeft: wp('5%')
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  }
});

export default List;
