import { AntDesign, FontAwesome, FontAwesome5 } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Icon = ({ iconName }) => {
  return (
    <View>
      <View style={styles.iconbg}>
        <InsetShadow>
          <View style={styles.inner}>
            {iconName == "snapchat-ghost" ? (
              <FontAwesome name={iconName} size={rf(18)} color="#2ECC71" />
            ) : iconName == "google" ? (
              <AntDesign name={iconName} size={rf(18)} color="#2ECC71" />
            ) : (
              <FontAwesome5 name={iconName} size={rf(18)} color="#2ECC71" />
            )}
          </View>
        </InsetShadow>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  iconbg: {
    height: hp("4%"),
    width: hp("4%"),
    borderRadius: 10,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    elevation: 2,
    marginLeft: 2
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  }
});

export default Icon;
