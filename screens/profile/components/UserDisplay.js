import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const UserDisplay = (props) => {
  const Gradient = () => {
    return (
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
    );
  };
  return (
    <View style={[styles.container, styles.shadow]}>
      <InsetShadow>
        <View style={styles.inner}>
          <View style={[styles.cont1, styles.shadow]}>
            <InsetShadow>
              <Gradient />
              <View style={styles.cont1Inner}>
                <View style={[styles.imgCont, styles.shadow]}>
                  <InsetShadow>
                    <Gradient />
                    <View style={styles.imgContInner}>
                      <View style={styles.displayPic}>
                        <InsetShadow elevation={10}>
                          <LinearGradient
                            // Background Linear Gradient
                            colors={["#FFFFFF", "#E3EDF7"]}
                            style={styles.background}
                          />
                          <View style={styles.imgInnerCont}>
                            {props.text.photo != null ? (
                              <Image
                                source={{uri:props.text.photo}}
                                resizeMode="cover"
                                style={styles.img}
                              />
                            ) : (
                              <Image
                                source={require("../../../assets/user.png")}
                                resizeMode="cover"
                                style={styles.img}
                              />
                            )}
                          </View>
                        </InsetShadow>
                      </View>
                    </View>
                  </InsetShadow>
                </View>
                <View style={styles.txtCont}>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={styles.name}>{props.text.name} </Text>
                    <Image
                      source={require("../../../assets/tick.png")}
                      resizeMode="cover"
                      style={styles.tick}
                    />
                  </View>
                  <Text style={styles.userName}>{props.text.username}</Text>
                </View>
              </View>
            </InsetShadow>
          </View>
          <View style={styles.details}>
            <View style={styles.detailsInner}>
              <Text style={styles.txt}>1000</Text>
              <Text style={styles.txt}>{props.text.followers}</Text>
            </View>
            <View style={styles.detailsInner}>
              <Text style={styles.txt}>50</Text>
              <Text style={styles.txt}>{props.text.ads}</Text>
            </View>
          </View>
        </View>
      </InsetShadow>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: wp("86%"),
    height: hp("22%"),
    alignSelf: "center",
    backgroundColor: "#2ECC71",
    marginTop: 10,
    borderRadius: 20,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  inner: {
    width: "100%",
    height: "100%",
  },
  cont1: {
    width: "100%",
    height: "70%",
    backgroundColor: "lightgreen",
    borderRadius: 15,
    overflow: "hidden",
  },
  cont1Inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: "5%",
  },
  imgCont: {
    width: hp("11%"),
    height: hp("11%"),
    borderRadius: 10,
    overflow: "hidden",
  },
  imgContInner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  displayPic: {
    height: hp("9%"),
    width: hp("9%"),
    borderRadius: 100,
    borderColor: "#fff",
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
  },
  imgInnerCont: {
    height: hp("9%"),
    width: hp("9%"),
    alignItems: "center",
    justifyContent: "center",
  },
  img: {
    height: "100%",
    width: "100%",
  },
  txtCont: {
    width: "70%",
    paddingHorizontal: "8%",
  },
  name: {
    fontSize: rf(16),
    fontWeight: "bold",
    paddingRight: 5,
  },
  tick: {
    width: wp("5%"),
    height: wp("5%"),
  },
  userName: {
    fontSize: rf(11),
    fontWeight: "bold",
    color: "#2ECC71",
    textAlign: "left",
  },
  details: {
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    width: "100%",
    height: "30%",
  },
  detailsInner: {
    alignItems: "center",
    justifyContent: "center",
    width: "40%",
  },
  txt: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: rf(14),
  },
  shadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
});

export default UserDisplay;
