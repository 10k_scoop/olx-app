import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ScrollView,
  View,
} from "react-native";
import Header from "./components/Header";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import SettingCard from "./components/SettingCard";

import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Settings = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Header
        screen="settings"
        onBackPress={() => props.navigation.goBack()}
        onBellPress={() => props.navigation.navigate("Notification")}
        title={Strings("Settings",lang)}
      />
      <View style={styles.Cont1}>
        <InsetShadow>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
            start={[1.0, 0.5]}
            end={[0.0, 0.1]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <ScrollView style={{ flex: 1 }}>
            <View style={styles.Inner}>
              <SettingCard iconName="language" txt={Strings("Language",lang)} txt1="English" />
              <SettingCard
                iconName="Country"
                txt={Strings("Country", lang)}
                txt1="Saudi Arabia"
              />
              <SettingCard
                iconName="moon"
                txt={Strings("darkTheme", lang)}
                switchBtn
              />
              <SettingCard
                iconName="bell"
                arrow
                txt={Strings("Notifications", lang)}
              />
              <SettingCard
                iconName="Block"
                arrow
                txt={Strings("blockedUsers", lang)}
              />
              <SettingCard
                iconName="Privacy"
                arrow
                txt={Strings("termsOfUse", lang)}
              />
              <SettingCard
                iconName="Privacy"
                arrow
                txt={Strings("privacyPolicy", lang)}
              />
              <SettingCard iconName="Help" arrow txt={Strings("Help", lang)} />
              <SettingCard
                iconName="Contact"
                arrow
                txt={Strings("contactUs", lang)}
              />
              <SettingCard
                arrow
                txt={Strings("aboutThisApp", lang)}
                iconName="info"
              />
            </View>
          </ScrollView>
        </InsetShadow>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: StatusBar.currentHeight,
    alignItems: "center",
    flex: 1,
  },
  Cont1: {
    width: wp("90%"),
    maxHeight: hp("86%"),
    borderRadius: 15,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  Inner: {
    width: wp("90%"),
    flex: 1,
    borderRadius: 10,
    alignItems: "center",
    marginBottom: hp("1%"),
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Settings);
