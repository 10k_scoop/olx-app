import React, { useState } from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Switch,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {
  AntDesign,
  Entypo,
  FontAwesome,
  Foundation,
  Ionicons,
  MaterialIcons,
} from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function SettingCard({
  txt,
  onPress,
  iconName,
  arrow,
  switchBtn,
  txt1,
  Icon,
}) {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);
  return (
    <View style={styles.cont1} onPress={onPress}>
      <LinearGradient
        // Background Linear Gradient
        colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
        start={[0.0, 0.5]}
        end={[1.0, 0.5]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
      <View style={styles.Inner}>
        <TouchableOpacity style={styles.Inner1}>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.icon}>
            {iconName == "language" ? (
              <MaterialIcons name="language" size={rf(22)} color="#2ECC71" />
            ) : iconName == "moon" ? (
              <Entypo name="moon" size={rf(22)} color="#2ECC71" />
            ) : iconName == "bell" ? (
              <FontAwesome name="bell" size={rf(22)} color="#2ECC71" />
            ) : iconName == "Country" ? (
              <MaterialIcons name="emoji-flags" size={rf(22)} color="#2ECC71" />
            ) : iconName == "Block" ? (
              <Entypo name="block" size={rf(22)} color="#2ECC71" />
            ) : iconName == "Privacy" ? (
              <AntDesign name="profile" size={rf(22)} color="#2ECC71" />
            ) : iconName == "Help" ? (
              <Entypo name="help-with-circle" size={rf(22)} color="#2ECC71" />
            ) : iconName == "Contact" ? (
              <Ionicons name="call" size={rf(22)} color="#2ECC71" />
            ) : <Foundation name="info" size={rf(22)} color="#2ECC71" />}
          </View>
          <View style={{paddingHorizontal:10,}}>
            <Text style={{ fontSize: rf(12) }}>{txt}</Text>
          </View>
        </TouchableOpacity>
        {arrow ? (
          <TouchableOpacity style={styles.Arrow}>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
              start={[1.2, 0.5]}
              end={[0.2, 0.3]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <Ionicons name="chevron-forward" size={24} color="#2ECC71" />
          </TouchableOpacity>
        ) : switchBtn ? (
          <Switch
            trackColor={{ false: "#767577", true: "#2ECC71" }}
            thumbColor={isEnabled ? "#2ECC71" : "#fff"}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}
          />
        ) : (
          <TouchableOpacity style={styles.Inner2}>
            <Text style={{ fontSize: rf(12), color: "#fff" }}>{txt1}</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("85%"),
    height: hp("7%"),
    borderRadius: 10,
    overflow: "hidden",
    marginTop: 15,
    marginHorizontal: wp("2%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  Inner: {
    textAlign: "center",
    fontSize: rf(13),
    width: wp("85%"),
    height: hp("7%"),
    textAlignVertical: "center",
    overflow: "hidden",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10,
  },
  Inner1: {
    minWidth: "40%",
    height: "60%",
    borderRadius: 10,
    overflow: "hidden",
    flexDirection: "row",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 2,
    justifyContent:'flex-start',
    
  },
  Inner2: {
    width: "30%",
    height: "60%",
    backgroundColor: "#2ECC71",
    justifyContent: "center",
    borderWidth: 0.6,
    borderColor: "#fff",
    alignItems: "center",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 2,
  },
  icon: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 5,
  },
  Arrow: {
    width: "12%",
    height: "70%",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
