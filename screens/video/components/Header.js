import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, FontAwesome } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import FilterBtn from "../../../components/FilterBtn";
export default function Header(props) {
  return (
    <View style={styles.cont1}>
      {/* back */}
      <View style={styles.logo}>
        <TouchableOpacity style={styles.bellIcon} onPress={props.onBackPress}>
          <AntDesign name="arrowleft" size={rf(16)} color="white" />
        </TouchableOpacity>
      </View>
      {/* buttons */}
      <View style={styles.cont2}>
        <FilterBtn txt={props.headerText.local} />
        <FilterBtn txt={props.headerText.follow} />
      </View>
      {/* Bell icon */}
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <TouchableOpacity style={styles.bellIcon}>
          <FontAwesome name="filter" size={rf(16)} color="white" />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    flexDirection: "row",
    alignItems: "center",
    height: hp("8%"),
    justifyContent: "space-evenly",
    paddingHorizontal: 5,
  },
  logo: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  cont2: {
    width: wp("65%"),
    height: hp("7%"),
    overflow: "hidden",
    marginHorizontal: "5%",
    flexDirection:"row",
    justifyContent:"space-evenly",
    alignItems:"center",
  },
  bellIcon: {
    width: wp("10%"),
    height: hp("5%"),
    borderRadius: 10,
    backgroundColor: "#2ECC71",
    alignItems: "center",
    justifyContent: "center"
  },
});
