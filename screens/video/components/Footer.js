import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import FilterBtn from '../../../components/FilterBtn';
import Button from './Button';

const Footer = (props) => {
    return (
      <View style={styles.cont}>
        <View style={styles.cont1}>
          <View style={styles.imgCont}>
            <InsetShadow elevation={10}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7"]}
                style={styles.background}
              />
              <View style={styles.imgInnerCont}>
                <Image
                  source={require("../../../assets/user.png")}
                  resizeMode="cover"
                  style={styles.img}
                />
              </View>
            </InsetShadow>
          </View>
          <View style={styles.txtCont}>
            <Text style={styles.name}>Elizabeth</Text>
          </View>
        </View>
        <Button txt={props.follow} />
        <Button txt={props.createVideo} />
      </View>
    );
}

//  <LinearGradient
//    // Background Linear Gradient
//    colors={["#FFFFFF", "#E3EDF7"]}
//    style={styles.background}
//  />

const styles = StyleSheet.create({
  cont: {
    width: wp("100%"),
    height: hp("9"),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: hp("2%"),
    marginTop: hp("1%")
  },
  cont1: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  imgCont: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    borderColor: "#fff",
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  imgInnerCont: {
    height: hp("8%"),
    width: hp("8%"),
    alignItems: "center",
    justifyContent: "center"
  },
  img: {
    height: "80%",
    width: "80%"
  },
  txtCont: {
    paddingLeft: wp("2%")
  },
  name: {
    fontSize: rf(14),
  }
});

export default Footer;
