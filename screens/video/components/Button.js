import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { FontAwesome, SimpleLineIcons } from "@expo/vector-icons";

export default function Button({ txt }) {
  return (
    <TouchableOpacity style={styles.cont1}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View
          style={[
            styles.Inner,
            { backgroundColor: txt == "Follow" && "#2ECC71" },
          ]}
        >
          <Text
            style={[styles.txt, { color: txt == "Follow" ? "#fff" : "#000" }]}
          >
            {txt}
          </Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    height: hp("4.5%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#fff",
    minWidth:wp('22%')
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  Inner: {
    flex: 1,
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "space-evenly",
    flexDirection: "row",
    minWidth:wp('22%')
  },
  txt: {
    fontSize: rf(13),
    paddingHorizontal: wp("4%"),
  },
});
