import {
  AntDesign,
  Entypo,
  FontAwesome,
  MaterialIcons,
} from "@expo/vector-icons";
import React from "react";
import {
  ImageBackground,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Footer from "./Footer";

const VideoCard = (props) => {
  return (
    <View style={styles.container}>
      <View style={[styles.bgImg, styles.shadow]}>
        <ImageBackground
          source={require("../../../assets/videobg.png")}
          resizeMode="cover"
          style={styles.inner}
        >
          <View style={styles.icons}>
            <TouchableOpacity>
              <Entypo
                name="dots-three-horizontal"
                size={rf(30)}
                color="#EDF3FA"
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <AntDesign name="heart" size={rf(30)} color="#EDF3FA" />
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: "center" }}>
              <MaterialIcons name="message" size={rf(30)} color="#EDF3FA" />
              <Text style={{ color: "#fff" }}>163</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{ alignItems: "center" }}>
              <FontAwesome name="share" size={rf(30)} color="#EDF3FA" />
              <Text style={{ color: "#fff" }}>{props.videoText.share}</Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </View>
      <View style={styles.txtCont}>
        <Text style={styles.txt}>{props.videoText.videoTitle}</Text>
        <View
          style={{ alignItems: "center", justifyContent: "center", flex: 1 }}
        >
          <Footer
            follow={props.videoText.share}
            createVideo={props.videoText.createVideo}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  bgImg: {
    width: wp("100%"),
    height: hp("75%"),
    borderRadius: 20,
    overflow: "hidden",
  },
  inner: {
    width: "100%",
    height: "100%",
    position: "absolute",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  icons: {
    width: "20%",
    height: "50%",
    justifyContent: "space-evenly",
    alignSelf: "flex-end",
    alignItems: "center",
  },
  txtCont: {
    width: wp("100%"),
    paddingHorizontal: wp("5%"),
    paddingTop: 10,
    height: hp("16%"),
  },
  txt: {
    fontSize: rf(16),
    fontWeight: "bold",
  },
  shadow: {
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
});

export default VideoCard;
