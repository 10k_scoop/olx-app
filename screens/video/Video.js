import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  Platform,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import VideoCard from "./components/VideoCard";
import Header from "./components/Header";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Video = (props) => {
  const videos = [1, 2, 3, 45, 6, 7, 8];
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <Header
        headerText={{
          local: Strings("Local", lang),
          follow: Strings("Follow", lang),
        }}
        onBackPress={() => props.navigation.goBack()}
      />
      <ScrollView
        horizontal={false}
        decelerationRate={Platform.OS == "ios" ? 0.66 : 0.6}
        snapToInterval={hp("91%")}
        snapToAlignment={"center"}
      >
        {videos.map((item, index) => (
          <VideoCard
            videoText={{
              share: Strings("Share", lang),
              createVideo: Strings("createVideo", lang),
              videoTitle:Strings("videoTitle",lang)
            }}
            key={index}
          />
        ))}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Video);
