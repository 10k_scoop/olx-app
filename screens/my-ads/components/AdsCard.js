import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  Image,
  ImageBackground,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import AdBtn from "./AdBtn";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function AdsCard({ location,title,adStatus, onPress, expire, active, buttonText,views }) {
  return (
    <TouchableOpacity style={styles.Card} onPress={onPress}>
      <View style={styles.FirstRow}>
        <InsetShadow elevation={8}>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={{ width: "100%", height: "60%" }}>
            <View style={styles.Inner1}>
              <View
                style={[
                  styles.ActiveBtn,
                  { backgroundColor: expire ? "#DC627B" : "#2ECC71" },
                ]}
              >
                <Text style={{ color: "#fff", fontSize: rf(14) }}>{adStatus}</Text>
              </View>
            </View>
            <View style={styles.Inner2}>
              <View style={styles.img}>
                <Image
                  style={{ width: "80%", height: "100%" }}
                  source={require("../../../assets/CategoryPic.png")}
                  resizeMode="center"
                />
              </View>
              <View style={styles.details}>
                <Text
                  style={{
                    fontSize: rf(18),
                    color: "#2ECC71",
                    fontWeight: "700",
                  }}
                >
                  {title}
                </Text>
                <Text style={{ marginLeft: 10,textAlign: "center"}}>{buttonText.subCategory} - {buttonText.location}</Text>
              </View>
            </View>
          </View>
        </InsetShadow>
      </View>

      <View style={styles.SecondRow}>
        <AdBtn txt={buttonText.edit} />
        <AdBtn txt={buttonText.delete} />
        <AdBtn txt={buttonText.share} share />
        <AdBtn txt={buttonText.boost} />
      </View>
      <View style={styles.ThirdRow}>
        <View
          style={[
            styles.Status,
            {
              backgroundColor: expire ? "transparent" : "#fff",
            },
          ]}
        >
          <Text style={{ fontSize: rf(11), fontWeight: "700" }}>
            {buttonText.status}
          </Text>
        </View>
        <View style={styles.StatusBar}>
          <View style={[styles.ProgressBar, { width: "45%" }]}></View>
        </View>
      </View>
      <View style={styles.LastRow}>
        <Text style={{ fontSize: rf(11) }}>
          {views} {buttonText.people + " " + buttonText.saw + " " + buttonText.yesterday}
        </Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  Card: {
    width: wp("95%"),
    height: hp("36%"),
    borderRadius: 10,
    backgroundColor: "#2ECC71",
    overflow: "hidden",
    marginBottom: 15,
  },
  FirstRow: {
    width: "100%",
    height: "55%",
    borderRadius: 10,
    overflow: "hidden",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
    borderRadius: 10,
  },
  Inner1: {
    width: "100%",
    height: "40%",
    justifyContent: "flex-end",
    alignItems: "flex-end",
    paddingHorizontal: 10,
  },
  ActiveBtn: {
    width: "22%",
    height: "80%",
    backgroundColor: "#2ECC71",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  Inner2: {
    width: "100%",
    height: "100%",
    marginTop: 10,
    flexDirection: "row",
  },
  img: {
    width: "40%",
    height: "100%",
    alignItems: "flex-end",
  },
  details: {
    width: "60%",
    height: "80%",
    justifyContent: "space-around",
  },
  SecondRow: {
    width: "100%",
    height: "20%",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  ThirdRow: {
    width: "100%",
    height: "13%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  Status: {
    width: "15%",
    height: "60%",
    backgroundColor: "#fff",
    borderRadius: 100,
    justifyContent: "center",
    alignItems: "center",
  },
  StatusBar: {
    width: "65%",
    height: "20%",
    borderRadius: 100,
    backgroundColor: "#fff",
    overflow: "hidden",
    borderWidth: 1,
    borderColor: "#fff",
  },
  LastRow: {
    alignItems: "center",
    width: "65%",
    left: "30%",
  },
  ProgressBar: {
    position: "absolute",
    height: "100%",
    backgroundColor: "#2ECC71",
    borderRadius: 100,
  },
});
