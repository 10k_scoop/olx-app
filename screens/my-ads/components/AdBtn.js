import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";

export default function AdBtn({ txt,onPress,share }) {
  return (
    <TouchableOpacity style={styles.cont1} onPress={onPress}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.Inner}>
          {share && <Entypo name="share" size={rf(16)} color="#2ECC71" />}
          <Text style={[styles.txt,{marginHorizontal:5}]}>{txt}</Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("20%"),
    height: hp("4%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row"
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  Inner: {
    flex: 1,
    flexDirection: "row",
    width: wp("20%"),
    height: hp("5%"),
    alignItems:"center",
    justifyContent:"center"
  },
  txt: {
    fontSize: rf(12),
    fontWeight:'600'
  }
});
