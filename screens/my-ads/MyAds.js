import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ScrollView,
  View,
  ActivityIndicator,
} from "react-native";
import AdsCard from "./components/AdsCard";
import Header1 from "../../components/Header1";
import { LinearGradient } from "expo-linear-gradient";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { getLang } from "../../state-management/actions/lang/Main";
import { getProducts } from "../../state-management/actions/products/actions";
import { connect } from "react-redux";
import Strings from "../../constants/lng/LocalizationStrings";
import * as firebase from "firebase";

const MyAds = (props) => {
  const [lang, setLanguage] = useState();
  const db = firebase.firestore();
  const [ads, setAds] = useState();
  const [loading, setLoading] = useState(true);
  const user = firebase.auth().currentUser;

  useEffect(() => {
    props?.getLang(setLanguage);
    props?.getProducts(db, setLoading);
  }, []);

  useEffect(() => {
    setAds(props.get_products);
  }, [props]);

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
        start={[1.5, 0]}
        end={[0.5, 1.1]}
        locations={[0.1, 31, 21]}
        style={styles.background}
      />
      <Header1
        screen="Ads"
        onBackPress={() => props.navigation.goBack()}
        onBellPress={() => props.navigation.navigate("Notification")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        {ads.map((item, index) => {
          if (item.addOwner == user?.providerData[0]?.email) {
            console.log(item)
            return (
              <AdsCard
                key={index}
                adStatus={
                  item.status
                    ? Strings("Active", lang)
                    : Strings("Expired", lang)
                }
                onPress={() => props.navigation.navigate("ProductDetails")}
                expire={item.status ? false : true}
                buttonText={{
                  edit: Strings("Edit", lang),
                  delete: Strings("Delete", lang),
                  share: Strings("Share", lang),
                  boost: Strings("Boost", lang),
                  status: Strings("Status", lang),
                  saw: Strings("Saw", lang),
                  people: Strings("People", lang),
                  yesterday: Strings("Yesterday", lang),
                  subCategory: item.subCategory,
                  location: item.location,
                }}
                views={item.peopleReached}
                title={item.title}
                image={item.image}
                price={item.price}
              />
            );
          }
        })}
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    alignItems: "center",
    paddingBottom: 0,
  },
  background: {
    position: "absolute",
    width: wp("100%"),
    height: hp("100%") + StatusBar.currentHeight,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
  get_products: state.main.get_products,
});
export default connect(mapStateToProps, { getLang, getProducts })(MyAds);
