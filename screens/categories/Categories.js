import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import { Dimensions, StatusBar, StyleSheet, View } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import Header1 from "../../components/Header1";
import Navigation from "../../components/Navigation";
import Card from "./components/Card";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Categories = (props) => {
  const cards = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
  ];
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <Header1
        screen={"category"}
        active="search"
        onBackPress={() => props.navigation.goBack()}
      />
      <Navigation
        active="category"
        onSearchPress={() => props.navigation.navigate("Search")}
        text={{
          search: Strings("Search", lang),
          categories: Strings("Categories", lang),
        }}
      />
      <View style={styles.card}>
        {cards.map((num) => (
          <Card key={num} />
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
  card: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-evenly",
    paddingTop: hp("5%"),
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Categories);
