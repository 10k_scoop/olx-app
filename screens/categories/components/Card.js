import { FontAwesome5 } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const Card = ({icon, name}) => {
    return (
      <TouchableOpacity style={styles.container}>
        <InsetShadow>
          <LinearGradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.inner}>
            <FontAwesome5 name="headphones-alt" size={rf(30)} color="#2ECC71" />
            <Text style={{ fontSize:rf(13), fontWeight:"bold", paddingTop: wp('2%') }}>Gadget</Text>
          </View>
        </InsetShadow>
      </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
  container: {
    width: wp("22%"),
    height: wp("28%"),
    borderRadius: 15,
    marginBottom: hp("1%"),
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  background: {
    position: "absolute",
    width: "105%",
    height: "100%"
  }
});

export default Card;
