import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  View,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Receiver from "./components/Receiver";
import Sender from "./components/Sender";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Tips from "./components/Tips";
import AdsCard from "./components/AdsCard";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Conversation = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
        start={[0.1, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
      <Header onlineText={Strings("Online",lang)} onBackPress={() => props.navigation.goBack()} />
      <ScrollView>
        {/* Card For featured ads & tips */}
        <AdsCard faya={Strings("FAYA", lang)} />
        <Tips
          gt={Strings("GT", lang)}
          CTL1={Strings("CTL1", lang)}
          CTL2={Strings("CTL2", lang)}
          CTL3={Strings("CTL3", lang)}
        />
        {/* Card For featured ads & tip */}
        <View style={{ marginBottom: hp("1%") }}>
          <Sender
            yesterdayText={Strings("Yesterday", lang)}
            todayText={Strings("Today", lang)}
          />
          <Receiver
            yesterdayText={Strings("Yesterday", lang)}
            todayText={Strings("Today", lang)}
          />
          <Sender
            yesterdayText={Strings("Yesterday", lang)}
            todayText={Strings("Today", lang)}
          />
          <Receiver
            yesterdayText={Strings("Yesterday", lang)}
            todayText={Strings("Today", lang)}
          />
          <Sender
            yesterdayText={Strings("Yesterday", lang)}
            todayText={Strings("Today", lang)}
          />
          <Receiver
            yesterdayText={Strings("Yesterday", lang)}
            todayText={Strings("Today", lang)}
          />
        </View>
      </ScrollView>
      <Footer />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:
      Platform.OS == "android"
        ? StatusBar.currentHeight + 10
        : StatusBar.currentHeight + 10,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Conversation);
