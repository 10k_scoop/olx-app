import { AntDesign } from "@expo/vector-icons";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const Tips = (props) => {
  return (
    <View style={styles.tips}>
      <View style={styles.icon}>
        <AntDesign name="exclamationcircle" size={rf(25)} color="#2ECC71" style={{right:10}} />
        <Text style={styles.name}>{props.gt}</Text>
      </View>
      <Text style={[styles.adsText, { paddingTop: "3%" }]}>{props.CTL1}</Text>
      <Text style={styles.adsText}>{props.CTL2}</Text>
      <Text style={styles.adsText}>{props.CTL3}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  adsText: {
    fontSize: rf(12),
    textAlign: "center",
  },
  tips: {
    width: wp("80%"),
    alignItems: "center",
    justifyContent: "center",
    paddingTop: hp("2%"),
    alignSelf: "center",
  },
  icon: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default Tips;
