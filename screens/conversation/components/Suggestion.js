import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

const Suggestion = ({text}) => {
    return (
      <View style={styles.container}>
        <InsetShadow>
          <LinearGradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.5, 0.0]}
            end={[0.5, 1.0]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.cont2}>
            <Text style={styles.txt}>{text}</Text>
          </View>
        </InsetShadow>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    height: hp("3.5%"),
    overflow: "hidden",
    marginLeft: 3,
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 2
  },
  cont2: {
    borderRadius: 20,
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  txt: {
    fontSize: rf(13),
    paddingHorizontal: 12
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  }
});

export default Suggestion;
