import { AntDesign, Entypo, FontAwesome } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const Header = ({onBackPress,onlineText}) => {
  return (
    <View style={[styles.header, { paddingTop: hp("2%") }]}>
      <TouchableOpacity
        style={[styles.iconBackground, { flex: 0.06 }]}
        onPress={onBackPress}
      >
        <AntDesign name="arrowleft" size={rf(17)} color="#fff" />
      </TouchableOpacity>
      <View style={styles.cont1}>
        <View style={styles.imgCont}>
          <InsetShadow elevation={10}>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#E3EDF7"]}
              style={styles.background}
            />
            <View style={styles.imgInnerCont}>
              <Image
                source={require("../../../assets/user.png")}
                resizeMode="cover"
                style={styles.img}
              />
            </View>
          </InsetShadow>
        </View>
        <View style={styles.txtCont}>
          <Text style={styles.name}>Elizabeth</Text>
          <Text>{onlineText}</Text>
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          flex: 0.15,
        }}
      >
        <TouchableOpacity>
          <FontAwesome
            name="phone"
            size={rf(27)}
            color="#2ECC71"
            style={{ paddingRight: wp("2%") }}
          />
        </TouchableOpacity>
        <TouchableOpacity>
          <Entypo name="dots-three-vertical" size={rf(25)} color="#2ECC71" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp("6%"),
  },
  iconBackground: {
    backgroundColor: "#2ECC71",
    padding: "2%",
    borderRadius: 7,
  },
  cont1: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flex: 0.79,
    paddingLeft: hp("4%"),
  },
  imgCont: {
    height: hp("9%"),
    width: hp("9%"),
    borderRadius: 100,
    borderColor: "#fff",
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
  imgInnerCont: {
    height: hp("9%"),
    width: hp("9%"),
    alignItems: "center",
    justifyContent: "center",
  },
  img: {
    height: "80%",
    width: "80%",
  },
  txtCont: {
    paddingLeft: hp("2%"),
  },
  name: {
    fontSize: rf(16),
    fontWeight: "bold",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
});

export default Header;
