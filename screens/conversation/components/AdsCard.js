import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const AdsCard = (props) => {
  return (
    <View style={styles.ads}>
      <InsetShadow>
        <LinearGradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.5, 0.0]}
          end={[0.5, 1.0]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.adsInner}>
          <Text style={styles.adsText}>{props.faya}</Text>
        </View>
      </InsetShadow>
    </View>
  );
};

const styles = StyleSheet.create({
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  adsText: {
    fontSize: rf(12),
    textAlign: "center",
  },
  ads: {
    marginTop: hp("3%"),
    width: wp("94%"),
    height: hp("20%"),
    alignSelf: "center",
    borderRadius: 15,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
  adsInner: {
    width: wp("94%"),
    height: hp("20%"),
    alignItems: "center",
    justifyContent: "center",
  },
  adsText: {
    fontSize: rf(12),
    textAlign: "center",
  },
 
});

export default AdsCard;
