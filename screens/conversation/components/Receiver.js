import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const Receiver = (props) => {
  return (
    <View style={styles.container}>
      {/* <View style={styles.triangle}></View> */}
      <View style={styles.cont2}>
        <InsetShadow>
          <View style={styles.cont2Inner}>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#f8f8f8"]}
              style={styles.background}
            />
            <View style={styles.textCont}>
              <Text style={styles.text}>Hi</Text>
              <Text style={styles.time}>{props.todayText}</Text>
            </View>
          </View>
        </InsetShadow>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: wp("45%"),
    height: hp("13%"),
    alignSelf: "flex-start",
    marginTop: hp("1%"),
    marginLeft: hp("1%"),
    overflow: "hidden",
    borderRadius:10
  },
  triangle: {
    width: "15%",
    height: "100%",
    borderLeftWidth: 30,
    borderTopWidth: 25,
    borderTopColor: "transparent",
    borderLeftColor: "#fff",
    alignSelf: "flex-start",
    position: "absolute",
    overflow: "hidden",
  },
  cont2: {
    marginTop: hp("2.5%"),
    position: "absolute",
    width: "100%",
    height: hp("10.5%"),
    borderTopRightRadius: 10,
    overflow: "hidden",
  },
  cont2Inner: {
    width: "100%",
    height: "100%",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  textCont: {
    height: "100%",
    justifyContent: "space-around",
    paddingHorizontal: hp("2%"),
  },
  text: {
    color: "#2ECC71",
    fontSize: rf(18),
    alignSelf: "flex-end",
  },
  time: {
    fontSize: rf(13),
    alignSelf: "flex-start",
  },
});

export default Receiver;
