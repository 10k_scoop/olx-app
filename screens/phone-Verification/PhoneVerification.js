import React, { useState } from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import CountryPicker from "react-native-country-picker-modal";
import { CountryCode, Country } from "./components/types";

export default function PhoneVerification() {
  const [countryCode, setCountryCode] = useState("US");
  const [country, setCountry] = useState();
  const [withFlag, setWithFlag] = useState(true);
  const [withCallingCode, setWithCallingCode] = useState(true
    );
  const [withFilter, setWithFilter] = useState(true);
  

  const onSelect = (country) => {
    setCountryCode(country.cca2);
    setCountry(country);
  };

  return (
    <View style={styles.cont}>
      <CountryPicker
        {...{
          countryCode,
          withFlag,
          withCallingCode,
          onSelect,
          withFilter,
        }}
        visible={false}
        containerButtonStyle={{
          height: "100%",
          width: "100%",
          justifyContent: "center",
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  cont: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
