import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect }from "react";
import { Dimensions, StatusBar, StyleSheet, Text, View } from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header1 from "../../components/Header1";
import List from "./components/List";
import Navigation from "../../components/Navigation";
import Suggestion from "./components/Suggestion";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Search = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  const Gradient = () => {
    return (
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
    );
  };
  const sugg = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  return (
    <View style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.bodybg}
      />
      <Header1
        active="search"
        screen={"search"}
        onBackPress={() => props.navigation.goBack()}
      />
      <Navigation
        active="search"
        onCategoryPress={() => props.navigation.navigate("Categories")}
        text={{
          search: Strings("Search", lang),
          categories: Strings("Categories", lang),
        }}
      />
      <View style={styles.res}>
        <View style={{ flex: 0.7 }}>
          <Text style={[styles.resText,{textAlign: "left",left:10}]}>{Strings("recentSearch", lang)}</Text>
        </View>
        <View style={{ flex: 0.3, alignItems: "center" }}>
          <Text style={{ fontSize: rf(13) }}>{Strings("seeMore", lang)}</Text>
        </View>
      </View>
      <List />
      <List />
      <List />
      <View style={[styles.adSpace, , styles.shadow]}>
        <InsetShadow>
          <Gradient />
        </InsetShadow>
      </View>
      <View style={styles.res}>
        <Text style={styles.resText}>{Strings("Suggestion", lang)}</Text>
      </View>
      <View style={styles.txtCont}>
        {sugg.map((num) => (
          <Suggestion key={num} txt={"Air Jordan"} />
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  bodybg: {
    position: "absolute",
    width: "100%",
    height: Dimensions.get("window").height + +StatusBar.currentHeight + 10,
  },
  res: {
    width: wp("90%"),
    height: hp("7%"),
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: wp("4%"),
  },
  resText: {
    fontWeight: "bold",
    fontSize: rf(16),
  },
  adSpace: {
    width: wp("100%"),
    height: hp("30%"),
    marginTop: hp("1%"),
    alignSelf: "center",
    borderRadius: 10,
    overflow: "hidden",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  txtCont: {
    flexWrap: "wrap",
    width: wp("96%"),
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
  },
  shadow: {
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Search);
