import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

const Suggestion = ({txt}) => {
  return (
    <TouchableOpacity style={styles.cont1}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.Inner}>
          <Text style={styles.txt} >
            {txt}
          </Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cont1: {
    width: wp("18%"),
    height: hp("4.5%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 7,
    marginBottom: wp('1%')
  },
  background: {
    position: "absolute",
    width: "105%",
    height: "100%"
  },
  Inner: {
    flex: 1,
    width: wp("18%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row"
  },
  txt: {
    fontSize: rf(12),
    fontWeight: "600",
    marginHorizontal: 5
  }
});

export default Suggestion;
