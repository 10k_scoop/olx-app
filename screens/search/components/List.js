import { Entypo, FontAwesome } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import FilterBtn from '../../../components/FilterBtn';

const List = () => {
    return (
      <View style={styles.container}>
        <View style={styles.cont2}>
          <Entypo
            name="back-in-time"
            size={rf(24)}
            color="#2ECC71"
            style={{ transform: [{ rotateY: "180deg" }] }}
          />
          <View style={styles.textCont}>
            <FilterBtn txt="Nike jogger" />
          </View>
        </View>
        <View style={{ flex: 0.3, alignItems: "center" }}>
          <View style={styles.icon}>
            <InsetShadow>
              <TouchableOpacity style={styles.inner}>
                <FontAwesome name="bell" size={rf(16)} color="#2ECC71" />
              </TouchableOpacity>
            </InsetShadow>
          </View>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("7%"),
    alignSelf: "center",
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: wp("4%")
  },
  icon: {
    backgroundColor: "#fff",
    height: hp("5%"),
    width: hp("5%"),
    borderRadius: 10,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  cont2: {
    flex: 0.7,
    alignItems: "center",
    flexDirection: "row"
  },
  textCont: {
    marginLeft: wp("3%")
  }
});

export default List;
