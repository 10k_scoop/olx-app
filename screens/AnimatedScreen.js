import React, { useEffect, useRef, useState } from "react";
import {
  Animated,
  Image,
  Dimensions,
  Easing,
  StyleSheet,
  View,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function AnimatedScreen({ navigation }) {
  const animatedValue = useRef(new Animated.Value(0)).current;
  const [isTop, setIsTop] = useState(true);
  const duration = 1500;
  const startAnimation = (toValue) => {
    Animated.loop(
      Animated.sequence([
        Animated.timing(animatedValue, {
          toValue: -hp("95%"),
          duration: duration,
          useNativeDriver: true,
        }),
        Animated.timing(animatedValue, {
          toValue: -hp("295%"),
          duration: 400,
          delay: 700,
          useNativeDriver: true,
        })
      ]),{iterations:1}
    ).start((endState) => {
      if (endState.finished) {
        //navigation.navigate("ProductHome");
      }
    });
  };

  useEffect(() => {
    startAnimation();
  }, []);

  return (
    <View style={styles.container}>
      <Animated.Image
        easing="ease-out"
        iterationCount={2}
        direction="alternate"
        source={require("../assets/animateBG.png")}
        style={[styles.square, { transform: [{ translateY: animatedValue }] }]}
      ></Animated.Image>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#2ECC71",
  },
  square: {
    height: "100%",
    width: "100%",
    transform: [{ translateY: 10 }],
    top: hp("100%"),
    opacity: 0.5,
  },
});
