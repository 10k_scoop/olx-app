import React, { useState, useEffect } from "react";
import {
  Platform,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { AntDesign, Entypo, Ionicons } from "@expo/vector-icons";
import ChatList from "./components/ChatList";
import SearchBox from "./components/SearchBox";
import { LinearGradient } from "expo-linear-gradient";
import BottomMenu from "../../components/BottomMenu";
import FilterBtn from "../../components/FilterBtn";
import Header from "./components/Header";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Chat = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <KeyboardAwareScrollView>
      <View style={styles.container}>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7"]}
          style={styles.background}
        />
        <Header onBellPress={() => props.navigation.navigate("Notification")} />
        <View style={[styles.header, { paddingTop: hp("2%") }]}>
          <TouchableOpacity
            style={styles.iconBackground}
            onPress={() => props.navigation.goBack()}
          >
            <AntDesign name="arrowleft" size={rf(20)} color="#fff" />
          </TouchableOpacity>
          <SearchBox searchPlaceholder={Strings("SIYC",lang)} />
        </View>
        <View style={styles.tags}>
          <FilterBtn txt={Strings("AllAds",lang)} />
          <FilterBtn txt={Strings("Unread",lang)} />
          <FilterBtn txt={Strings("Deleted",lang)}/>
        </View>
        <View style={styles.list}>
          <ChatList
            onConvPress={() => props.navigation.navigate("Conversation")}
            yesterdayText={Strings("Today",lang)}
            todayText={Strings("Yesterday",lang)}
            sneakerText={Strings("Sneaker",lang)}
          />
          <ChatList
            onConvPress={() => props.navigation.navigate("Conversation")}
            yesterdayText={Strings("Today",lang)}
            todayText={Strings("Yesterday",lang)}
            sneakerText={Strings("Sneaker",lang)}
          />
          <ChatList
            onConvPress={() => props.navigation.navigate("Conversation")}
            yesterdayText={Strings("Today",lang)}
            todayText={Strings("Yesterday",lang)}
            sneakerText={Strings("Sneaker",lang)}
          />
         
        </View>
        <BottomMenu
          active="chat"
          onHomePress={() => props.navigation.navigate("ProductHome")}
          onAddPostPress={() => props.navigation.navigate("AddPost")}
          onProfilePress={() => props.navigation.navigate("Profile")}
          onVideoPress={() => props.navigation.navigate("Video")}
        />
      </View>
    </KeyboardAwareScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:
      Platform.OS == "android"
        ? StatusBar.currentHeight + hp("3%")
        : StatusBar.currentHeight + hp("5%"),
    height: hp("100%") + StatusBar.currentHeight,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: wp("4%"),
  },
  iconBackground: {
    backgroundColor: "#2ECC71",
    padding: "2%",
    borderRadius: 10,
  },
  tags: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingTop: hp("2%"),
    paddingHorizontal: wp("6%"),
  },
  list: {
    width: "100%",
    justifyContent: "flex-start",
    paddingTop: hp("2%"),
    flex: 1,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: hp("100%") + StatusBar.currentHeight,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Chat);
