import React from 'react';
import { StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { AntDesign } from "@expo/vector-icons";
import { RFValue as rf } from 'react-native-responsive-fontsize';
import InsetShadow from 'react-native-inset-shadow';
import { LinearGradient } from 'expo-linear-gradient';

const SearchBox = ({searchPlaceholder}) => {
    return (
      <View style={styles.search}>
        <InsetShadow elevation={10}>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.container}>
            <View style={{ flex: 0.8, justifyContent: "center" }}>
              <TextInput placeholder={searchPlaceholder} />
            </View>
            <View style={styles.icon}>
              <TouchableOpacity style={styles.iconBackground}>
                  <AntDesign name="search1" size={rf(16)} color="#fff" />
              </TouchableOpacity>
            </View>
          </View>
        </InsetShadow>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: hp("1%")
  },
  icon: {
    flex: 0.2,
    alignItems: "flex-end"
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  iconBackground: {
    backgroundColor: "#2ECC71",
    borderRadius: 7,
    justifyContent: "center",
    alignItems: "center",
    height: "60%",
    width: "70%"
  },
  search: {
    width: wp("80%"),
    height: hp("7%"),
    borderRadius: 20,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  }
});

export default SearchBox;
