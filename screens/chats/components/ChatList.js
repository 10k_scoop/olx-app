import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import { Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const ChatList = ({ onConvPress,yesterdayText,todayText,sneakerText }) => {
  return (
    <View style={styles.container}>
      <View style={styles.icon}>
        <InsetShadow elevation={10}>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7"]}
            style={styles.background}
          />
          <View
            style={{
              height: hp("10%"),
              width: hp("10%"),
              alignItems: "center",
              justifyContent: "center",
            }}
          >
            <Image
              source={require("../../../assets/user.png")}
              resizeMode="contain"
              style={styles.img}
            />
          </View>
        </InsetShadow>
      </View>
      <View
        style={{
          width: wp("90%"),
          height: hp("10%"),
          alignItems: "flex-end",
          justifyContent: "flex-end",
          shadowColor: "#000",
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.25,
          shadowRadius: 3.84,

          elevation: 5,
        }}
      >
        <View style={styles.list}>
          <InsetShadow elevation={5}>
            <View style={styles.list}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7"]}
                style={styles.background}
              />
              <View style={styles.listDetails}>
                <TouchableOpacity onPress={onConvPress}>
                  <View
                    style={{
                      width: "85%",
                      height: "100%",
                      flexDirection: "row",
                    }}
                  >
                    <View
                      style={{
                        flex: 0.7,
                        justifyContent: "space-evenly",
                      }}
                    >
                      <Text style={styles.name}>Elizabeth</Text>
                      <Text style={styles.details}>Hello.</Text>
                      <Text style={styles.snearker}> {sneakerText}</Text>
                    </View>
                    <View
                      style={{
                        flex: 0.3,
                        justifyContent: "center",
                        alignItems: "center",
                      }}
                    >
                      <View style={styles.time}>
                        <Text style={{ color: "#fff" }}>1</Text>
                      </View>
                      <Text style={{fontSize:rf(11),top:5}}>{yesterdayText}</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </InsetShadow>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    width: wp("90%"),
    height: hp("10%"),
    alignSelf: "center",
    marginTop: hp("1.5%"),
  },
  icon: {
    height: hp("10%"),
    width: hp("10%"),
    borderRadius: 100,
    overflow: "hidden",
    borderWidth: 1,
    borderColor: "#fff",
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 999,
    position: "absolute",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  img: {
    width: "75%",
    height: "75%",
  },
  list: {
    width: wp("78%"),
    height: hp("10%"),
    alignItems: "flex-end",
    justifyContent: "flex-end",
    borderRadius: 20,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 3,
  },
  listDetails: {
    width: "100%",
    height: "100%",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#fff",
    alignItems: "flex-end",
    justifyContent: "flex-end",
  },
  name: {
    fontSize: rf(14),
    fontWeight: "bold",
  },
  details: {
    fontSize: rf(13),
  },
  snearker: {
    fontSize: rf(10),
    color: "#fff",
    backgroundColor: "#2ECC71",
    width: "30%",
    textAlign: "center",
    fontWeight: "bold",
  },
  time: {
    backgroundColor: "#2ECC71",
    width: hp("3%"),
    height: hp("3%"),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
});

export default ChatList;
