import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Entypo,Ionicons } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import InsetShadow from "react-native-inset-shadow";
import { LinearGradient } from "expo-linear-gradient";

const Header = ({onBellPress,onDotPress}) => {
  return (
    <View style={styles.header}>
      <Text style={styles.text}>Chats</Text>
      <View style={{ flexDirection: "row" }}>
        <TouchableOpacity onPress={onBellPress}>
          <Ionicons
            name="notifications"
            size={rf(18)}
            color="#2ECC71"
            style={{ paddingRight: wp("2%") }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={onDotPress}>
          <Entypo name="dots-three-vertical" size={rf(18)} color="#2ECC71" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: wp("6%"),
  },
  text: {
    fontSize: rf(20),
    fontWeight: "bold",
  },
});

export default Header;
