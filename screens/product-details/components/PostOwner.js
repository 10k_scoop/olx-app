import { MaterialIcons } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import InfoList from './InfoList';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Share from './Share';

const PostOwner = (props) => {
    return (
      <View style={[styles.info, styles.shadow]}>
        <InsetShadow>
          <LinearGradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.5, 0.0]}
            end={[0.5, 1.0]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.infoInner}>
            <View style={styles.cont1}>
              <View style={styles.imgCont}>
                <InsetShadow>
                  <View style={[styles.imgInnerCont]}>
                    <Image
                      source={require("../../../assets/user.png")}
                      resizeMode="cover"
                      style={{ height: "80%", width: "80%" }}
                    />
                  </View>
                </InsetShadow>
              </View>
              <View style={{ paddingLeft: hp("2%") }}>
                <Text style={styles.name}>Elizabeth</Text>
              </View>
            </View>
            <View style={styles.contact}>
              <View style={styles.contactInner}>
                <View style={styles.cont2}>
                  <InsetShadow>
                    <View style={styles.cont3}>
                      <MaterialIcons name="call" size={rf(22)} color="#fff" />
                      <Text style={[styles.name, { color: "#fff" }]}>
                        {"  "}00000
                      </Text>
                    </View>
                  </InsetShadow>
                </View>
                <Share iconName={"whatsapp"} />
                <View style={styles.cont2}>
                  <InsetShadow>
                    <View style={styles.cont3}>
                      <MaterialIcons
                        name="add-box"
                        size={rf(22)}
                        color="#fff"
                      />
                      <Text style={[styles.name, { color: "#fff",marginLeft:10 }]}>
                        {props.text.follow}
                      </Text>
                    </View>
                  </InsetShadow>
                </View>
              </View>
            </View>
            <InfoList name= {props.text.rt} detail={"1 "+props.text.hour} />
            <InfoList name= {props.text.ms} detail="24/7/2017" />
            <InfoList name= {props.text.rating} stars={true} />
          </View>
        </InsetShadow>
      </View>
    );
}

const styles = StyleSheet.create({
  info: {
    width: wp("94%"),
    height: hp("45%"),
    borderRadius: 20,
    alignSelf: "center",
    overflow: "hidden"
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  shadow: {
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  },
  infoInner: {
    width: "100%",
    height: "100%",
    paddingTop: "2%"
  },
  cont1: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: hp("2%")
  },
  imgCont: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    backgroundColor: "white",
    borderColor: "#fff",
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden"
  },
  imgInnerCont: {
    height: hp("8%"),
    width: hp("8%"),
    alignItems: "center",
    justifyContent: "center"
  },
  name: {
    fontSize: rf(16),
    fontWeight: "bold"
  },
  contact: {
    width: "94%",
    height: hp("6%"),
    alignSelf: "center",
    marginTop: "3%",
    overflow: "hidden"
  },
  contactInner: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    height: "100%",
    alignItems: "center"
  },
  cont2: {
    width: "40%",
    height: "100%",
    borderRadius: 15,
    overflow: "hidden",
    backgroundColor: "#2ECC71"
  },

  cont3: {
    width: "100%",
    height: "100%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  }
});

export default PostOwner;
