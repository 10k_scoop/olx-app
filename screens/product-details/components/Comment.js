import { Feather } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { Image, StyleSheet, Text, View } from "react-native";
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const Comment = () => {
    return (
      <View style={styles.container}>
        <View style={styles.imgCont}>
          <InsetShadow>
            <View style={[styles.imgInnerCont]}>
              <Image
                source={require("../../../assets/user.png")}
                resizeMode="cover"
                style={styles.img}
              />
            </View>
          </InsetShadow>
        </View>
        <View style={styles.cont1}>
          <InsetShadow>
            <LinearGradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.0, 0.5]}
              end={[1.0, 0.5]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <View style={styles.inner}>
              <View style={styles.txtCont}>
                <Text style={styles.name}>Elizabeth</Text>
                <Text style={styles.details}>Hello.</Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <Feather name="clock" size={rf(12)} color="#2ECC71" />
                  <Text style={{ fontSize: rf(11) }}>24-7-2021</Text>
                </View>
              </View>
            </View>
          </InsetShadow>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    height: hp("14%"),
    width: wp("90%"),
    alignSelf: "center",
    overflow: "hidden",
    marginBottom: hp("1%")
  },
  imgCont: {
    height: hp("8%"),
    width: hp("8%"),
    borderRadius: 100,
    backgroundColor: "white",
    borderColor: "#fff",
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    position: "absolute",
    zIndex: 9
  },
  imgInnerCont: {
    height: hp("8%"),
    width: hp("8%"),
    alignItems: "center",
    justifyContent: "center"
  },
  img: {
    height: "80%",
    width: "80%"
  },
  cont1: {
    width: "90%",
    height: "90%",
    alignSelf: "flex-end",
    marginTop: "3%",
    borderColor: "#fff",
    borderRadius: 15,
    borderWidth: 1,
    overflow: "hidden"
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  inner: {
    width: "100%",
    height: "100%"
  },
  name: {
    fontSize: rf(14),
    fontWeight: "bold"
  },
  details: {
    fontSize: rf(13)
  },
  txtCont: {
    paddingLeft: "12%",
    justifyContent: "space-evenly",
    height: "100%",
    width: "100%"
  }
});

export default Comment;
