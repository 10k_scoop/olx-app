import { Feather, FontAwesome, FontAwesome5, Ionicons } from '@expo/vector-icons';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp} from 'react-native-responsive-screen';

const Share = ({ iconName }) => {
  return (
    <View>
      <View style={styles.iconbg}>
        <InsetShadow>
          <View style={styles.inner}>
            {iconName == "message-circle" ? (
              <Feather name={iconName} size={rf(22)} color="#2ECC71" />
            ) : iconName == "mail" || iconName == "link" ? (
              <Ionicons name={iconName} size={rf(22)} color="#2ECC71" />
            ) : iconName == "send" ? (
              <FontAwesome name="send" size={rf(20)} color="#2ECC71" />
            ) : (
              <FontAwesome5 name={iconName} size={rf(20)} color="#2ECC71" />
            )}
          </View>
        </InsetShadow>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  iconbg: {
    height: hp("5%"),
    width: hp("5%"),
    borderRadius: 10,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    elevation: 2
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  }
});

export default Share;
