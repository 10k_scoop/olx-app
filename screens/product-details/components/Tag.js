import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const Tag = ({name}) => {
    return (
      <View style={styles.container}>
        <InsetShadow elevation={10}>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
            <View style={styles.tag}>
              <Text style={styles.text}>{"  " + name}</Text>
            </View>
        </InsetShadow>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    height: hp("5%"),
    width: "31%",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  tag: {
    justifyContent: "space-evenly",
    alignItems: "center",
    height: "100%",
    width: "100%",
    flexDirection: "row",
  },
  text: {
    fontSize: rf(12),
    fontWeight: "600"
  }
});

export default Tag;
