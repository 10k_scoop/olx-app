import { AntDesign } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { ImageBackground, StyleSheet, Text, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import Tag from './Tag';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const ProductImage = (props) => {
    return (
      <View style={[styles.product, styles.shadow]}>
        <View style={[styles.imagebg, styles.shadow]}>
          <InsetShadow>
            <View style={styles.inner}>
              <ImageBackground
                style={{
                  width: "100%",
                  height: "100%",
                  justifyContent: "space-between"
                }}
                source={require("../../../assets/bikebg.png")}
                resizeMode="cover"
              >
                <View style={styles.heart}>
                  <LinearGradient
                    colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                    start={[0.5, 0.0]}
                    end={[0.5, 1.0]}
                    locations={[0.2, 1, 1]}
                    style={styles.background}
                  />
                  <AntDesign name="heart" size={rf(18)} color="#2ECC71" />
                </View>
                <View style={[styles.pages, styles.shadow]}>
                  <InsetShadow>
                    <View style={styles.inner}>
                      <LinearGradient
                        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                        start={[0.5, 0.0]}
                        end={[0.5, 1.0]}
                        locations={[0.2, 1, 1]}
                        style={styles.background}
                      />
                      <Text style={styles.txt}>1/5</Text>
                    </View>
                  </InsetShadow>
                </View>
              </ImageBackground>
            </View>
          </InsetShadow>
        </View>
        <View style={styles.tags}>
          <Tag name={props.text.brand} />
          <Tag name={props.text.name} />
          <Tag name={props.text.brand} />
        </View>
        <View style={[styles.title, styles.shadow]}>
          <InsetShadow>
            <LinearGradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.5, 0.0]}
              end={[0.5, 1.0]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <View style={styles.inner}>
              <Text style={styles.titletxt}>{props.text.title}</Text>
            </View>
          </InsetShadow>
        </View>
        <View style={styles.tags}>
          <Tag name={props.text.location} />
          <Tag name={props.text.price+" $2000"} />
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  product: {
    width: wp("100%"),
    height: hp("60%"),
    backgroundColor: "#2ECC71",
    borderRadius: 15,
    overflow: "hidden"
  },
  imagebg: {
    width: "100%",
    height: "60%",
    alignSelf: "center",
    borderRadius: 15,
    overflow: "hidden",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  heart: {
    backgroundColor: "#fff",
    alignSelf: "flex-end",
    height: hp("6%"),
    width: hp("6%"),
    alignItems: "center",
    justifyContent: "center",
    overflow: "hidden",
    borderRadius: 100,
    marginTop: hp("2%"),
    marginRight: hp("2%"),
    shadowColor: "#fff",
    shadowOffset: {
      width: 1,
      height: 1
    },
    shadowOpacity: 0.9,
    elevation: 5
  },
  pages: {
    height: hp("4%"),
    width: hp("8%"),
    backgroundColor: "#fff",
    borderRadius: 20,
    overflow: "hidden",
    alignSelf: "flex-end",
    marginBottom: hp("2%"),
    marginRight: hp("2%"),
  },
  txt:{
    color: "#2ECC71",
    fontSize: rf(14),
    fontWeight: "bold"
    },
  title: {
    height: hp("7%"),
    width: "96%",
    marginTop: hp("1.5%"),
    alignSelf: "center",
    overflow: "hidden",
    borderRadius: 12
  },
  titletxt: {
    fontSize: rf(16),
    fontWeight: "bold"
  },
  tags: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    paddingTop: hp("1.5%"),
    paddingHorizontal: wp("2%")
  },
  shadow:{
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  }
});

export default ProductImage;
