import { AntDesign } from '@expo/vector-icons';
import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

const InfoList = ({name, detail, stars}) => {
    return (
      <View style={styles.container}>
        <InsetShadow>
          <LinearGradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.5, 0.0]}
            end={[0.5, 1.0]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.contInner}>
            <View style={styles.catagName}>
              <InsetShadow>
                <View style={styles.cont2}>
                  <LinearGradient
                    colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                    start={[0.5, 0.0]}
                    end={[0.5, 1.0]}
                    locations={[0.2, 1, 1]}
                    style={styles.background}
                  />
                  <Text style={styles.text}>{name}</Text>
                </View>
              </InsetShadow>
            </View>
            {stars ? (
              <View style={styles.catagName}>
                <InsetShadow>
                  <View
                    style={[
                      styles.cont2,
                      {
                        flexDirection: "row",
                        justifyContent: "space-evenly",
                        paddingHorizontal: "3%"
                      }
                    ]}
                  >
                    <LinearGradient
                      colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                      start={[0.5, 0.0]}
                      end={[0.5, 1.0]}
                      locations={[0.2, 1, 1]}
                      style={styles.background}
                    />
                    <AntDesign name="star" size={rf(16)} color="#2ECC71" />
                    <AntDesign name="star" size={rf(16)} color="#2ECC71" />
                    <AntDesign name="star" size={rf(16)} color="#2ECC71" />
                    <AntDesign name="star" size={rf(16)} color="#2ECC71" />
                    <AntDesign name="star" size={rf(16)} color="#2ECC71" />
                  </View>
                </InsetShadow>
              </View>
            ) : (
              <View style={[styles.catagName, { backgroundColor: "#2ECC71" }]}>
                <InsetShadow>
                  <View style={styles.cont2}>
                    <Text
                      style={[styles.text, { color: "#fff", fontSize: rf(14) }]}
                    >
                      {detail}
                    </Text>
                  </View>
                </InsetShadow>
              </View>
            )}
          </View>
        </InsetShadow>
      </View>
    );
}

const styles = StyleSheet.create({
  container: {
    width: "94%",
    height: hp("7.5%"),
    alignSelf: "center",
    marginTop: hp("1.5%"),
    borderRadius: 15,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    elevation: 7
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  contInner: {
    width: "100%",
    height: "100%",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    paddingHorizontal: "3%"
  },
  catagName: {
    height: "67%",
    overflow: "hidden",
    borderRadius: 10,
    borderColor: "#fff",
    borderWidth: 1,
    width: "42%"
  },
  cont2: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    alignSelf: "flex-start",
    paddingLeft: "10%",
    fontSize: rf(13)
  },
});

export default InfoList;
