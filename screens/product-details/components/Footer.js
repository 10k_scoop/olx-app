import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';
import Share from './Share';

const Footer = (props) => {
    return (
      <View style={styles.footer}>
        <InsetShadow>
          <LinearGradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={[styles.inner, { flexDirection: "row" }]}>
            <View
              style={[
                styles.inner,
                { flex: 0.8, alignItems: "flex-start", paddingLeft: "5%" }
              ]}
            >
              <TextInput
                placeholder={props.placeholder}
                style={{
                  width: "100%",
                  height: "50%"
                }}
              />
            </View>
            <View style={[styles.inner, { flex: 0.2 }]}>
              <Share iconName={"send"} />
            </View>
          </View>
        </InsetShadow>
      </View>
    );
}

const styles = StyleSheet.create({
  footer: {
    width: wp("100%"),
    height: hp("8%"),
    marginBottom: hp("1%"),
    borderRadius: 40,
    borderWidth: 1,
    borderColor: "#fff",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    elevation: 5
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  }
});

export default Footer;
