import React from 'react';
import { Image, StyleSheet, View } from "react-native";
import InsetShadow from 'react-native-inset-shadow';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

const Gallery = () => {
    return (
      <View>
        <View style={styles.galImg}>
          <InsetShadow>
            <View style={styles.inner}>
              <Image
                source={require("../../../assets/gallery.png")}
                resizeMode="cover"
                style={{ width: "100%", height: "100%" }}
              />
            </View>
          </InsetShadow>
        </View>
      </View>
    );
}

const styles = StyleSheet.create({
  galImg: {
    width: wp("31%"),
    height: wp("31%"),
    borderRadius: 15,
    borderColor: "#fff",
    overflow: "hidden",
    borderWidth: 1,
    marginBottom: hp("1%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    elevation: 5
  },
  inner:{
      width:"100%",
      height:"100%",
      alignItems:"center",
      justifyContent:"center"
  }
});

export default Gallery;
