import { AntDesign, MaterialIcons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import React, { useState, useEffect } from "react";
import {
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Category from "../../components/Category";
import Comment from "./components/Comment";
import Footer from "./components/Footer";
import Gallery from "./components/Gallery";
import InfoList from "./components/InfoList";
import PostOwner from "./components/PostOwner";
import ProductImage from "./components/ProductImage";
import Share from "./components/Share";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const ProductDetails = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  const Gradient = () => {
    return (
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity
          style={styles.iconBackground}
          onPress={() => props.navigation.goBack()}
        >
          <AntDesign name="arrowleft" size={rf(20)} color="#fff" />
        </TouchableOpacity>
      </View>
      <ScrollView>
        <ProductImage
          text={{
            brand: Strings("Brand", lang),
            model: Strings("Model", lang),
            name: Strings("Name", lang),
            title: Strings("Title", lang),
            location: Strings("Location", lang),
            price: Strings("Price", lang),
          }}
        />
        <View style={styles.cat}>
          <Category name={Strings("Information", lang)} />
        </View>
        <View style={[styles.info, styles.shadow]}>
          <InsetShadow>
            <Gradient />
            <View style={styles.infoInner}>
              <InfoList
                name={Strings("Category", lang)}
                detail={Strings("CAB", lang)}
              />
              <InfoList
                name={Strings("subCategory", lang)}
                detail={Strings("CFS", lang)}
              />
              <InfoList name={Strings("bikeMake", lang)} detail={Strings("Honda", lang)} />
              <InfoList name={Strings("Model", lang)} detail="CG 125" />
              <InfoList name={Strings("Year", lang)} detail="2021" />
              <InfoList name={Strings("kilometer", lang)} detail="0" />
              <InfoList
                name={Strings("Transmission", lang)}
                detail={Strings("Automatic", lang)}
              />
              <InfoList name={Strings("Fuel", lang)} detail={Strings("Gasoline", lang)} />
            </View>
          </InsetShadow>
        </View>
        <View style={[styles.adSpace, , styles.shadow]}>
          <InsetShadow>
            <Gradient />
          </InsetShadow>
        </View>
        <View style={[styles.info, styles.shadow, { height: hp("48%") }]}>
          <InsetShadow>
            <Gradient />
            <View style={styles.infoInner}>
              <InfoList name={Strings("Color", lang)} detail={Strings("Black", lang)} />
              <InfoList
                name={Strings("paymentMethod", lang)}
                detail={Strings("cashOnly", lang)}
              />
              <InfoList name={Strings("postId", lang)} detail="15666666" />
              <InfoList
                name={Strings("publishDate", lang)}
                detail={"2 "+Strings("hoursAgo", lang)}
              />
              <InfoList name={Strings("Price", lang)} detail={Strings("AFP", lang)} />
            </View>
          </InsetShadow>
        </View>
        <View style={styles.cat}>
          <Category name={Strings("Description", lang)} />
        </View>
        <View style={[styles.description, styles.shadow]}>
          <InsetShadow>
            <Gradient />
            <View style={styles.desc}>
              <Text style={styles.descText}>{Strings("dummyText", lang)}</Text>
            </View>
          </InsetShadow>
        </View>
        <View style={styles.cat}>
          <Category name={Strings("shareTheAd", lang)} />
        </View>
        <View style={[styles.socials, styles.shadow]}>
          <InsetShadow>
            <Gradient />
            <View style={styles.socialInner}>
              <Share iconName={"facebook-f"} />
              <Share iconName={"twitter"} />
              <Share iconName={"whatsapp"} />
              <Share iconName={"message-circle"} />
              <Share iconName={"mail"} />
              <Share iconName={"link"} />
            </View>
          </InsetShadow>
        </View>
        <View style={styles.cat}>
          <Category name={Strings("postOwner", lang)} />
        </View>
        <PostOwner
          text={{
            follow: Strings("Follow", lang),
            rt: Strings("responseTime", lang),
            ms:Strings("memberSince",lang),
            rating:Strings("Rating",lang),
            hour:Strings("Hour", lang)
          }}
        />
        <View style={styles.cat}>
          <Category name={Strings("moreAds", lang)} />
        </View>
        <View style={styles.gallery}>
          <Gallery />
          <Gallery />
          <Gallery />
          <Gallery />
          <Gallery />
          <Gallery />
        </View>
        <View style={[styles.addPost, styles.shadow]}>
          <InsetShadow>
            <Gradient />
            <View style={[styles.inner, { flexDirection: "row" }]}>
              <AntDesign name="camera" size={rf(24)} color="#2ECC71" />
              <Text style={[styles.titletxt, { color: "#2ECC71" }]}>
                {Strings("ASPF", lang)}
              </Text>
            </View>
          </InsetShadow>
        </View>
        <View style={styles.cat}>
          <Category name={Strings("Comments", lang)} />
        </View>
        <Comment />
        <Comment />
        <Footer placeholder={Strings("WYCH", lang)} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop:
      Platform.OS == "android"
        ? StatusBar.currentHeight + hp("1%")
        : StatusBar.currentHeight + hp("4%"),
  },
  header: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: wp("2%"),
    paddingBottom: hp("1%"),
  },
  iconBackground: {
    backgroundColor: "#2ECC71",
    padding: "2%",
    borderRadius: 10,
  },
  description: {
    alignSelf: "center",
    borderRadius: 15,
    overflow: "hidden",
    width: wp("94%"),
    height: hp("17%"),
    marginBottom: hp("2%"),
  },
  adSpace: {
    width: wp("100%"),
    height: hp("30%"),
    marginTop: hp("2%"),
    marginBottom: hp("2%"),
    alignSelf: "center",
    borderRadius: 15,
    overflow: "hidden",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  inner: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  addPost: {
    width: wp("88%"),
    height: hp("7%"),
    width: "96%",
    marginTop: hp("1.5%"),
    alignSelf: "center",
    overflow: "hidden",
    borderRadius: 12,
  },
  titletxt: {
    fontSize: rf(16),
    fontWeight: "bold",
  },
  cat: {
    width: "100%",
    paddingTop: hp("2.5%"),
    paddingBottom: hp("1.5%"),
    paddingHorizontal: wp("6%"),
  },
  info: {
    width: wp("94%"),
    height: hp("75.5%"),
    borderRadius: 20,
    alignSelf: "center",
    overflow: "hidden",
  },
  infoInner: {
    width: "100%",
    height: "100%",
    paddingTop: "2%",
  },
  desc: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    paddingHorizontal: "6%",
    paddingTop: "5%",
  },
  descText: {
    fontSize: rf(15),
  },
  socials: {
    width: wp("94%"),
    height: hp("8%"),
    alignSelf: "center",
    borderRadius: 15,
    overflow: "hidden",
    marginBottom: "3%",
  },
  socialInner: {
    width: "100%",
    height: "100%",
    justifyContent: "space-evenly",
    alignItems: "center",
    flexDirection: "row",
  },
  gallery: {
    flexDirection: "row",
    flexWrap: "wrap",
    justifyContent: "space-between",
    paddingHorizontal: wp("2%"),
    alignItems: "center",
  },
  shadow: {
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(ProductDetails);
