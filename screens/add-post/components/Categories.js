import { LinearGradient } from "expo-linear-gradient";
import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
} from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

const Categories = ({onChangeText, name, input, select, type, lang,selectPress }) => {
  return (
    <View
      style={[
        styles.container,
        {
          width: type == "title" || type == "addDescription" ? "100%" : "35%",
          height: type == "addDescription" ? hp("12%") : hp("5%"),
        },
      ]}
    >
      <InsetShadow>
        <LinearGradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.5, 0.0]}
          end={[0.5, 1.0]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        {select && (
          <TouchableOpacity
            style={[
              styles.tag,
              { alignItems: lang == "en" ? "flex-start" : "center" },
            ]}
            onPress={selectPress}
          >
            <Text style={[styles.text, { textAlign: "center" }]}>
              {"  " + name}
            </Text>
          </TouchableOpacity>
        )}
        {input && (
          <TextInput
            multiline={true}
            placeholderTextColor="#222"
            style={[
              styles.textInput,
              {
                textAlignVertical: type == "addDescription" ? "top" : "center",
                paddingVertical: type == "addDescription" ? 10 : 0,
                textAlign:
                  type != "addDescription" && lang == "ar"
                    ? "center"
                    : type == "title" && lang == "en"
                    ? "center"
                    : "auto",
              },
            ]}
            placeholder={name}
            onChangeText={(val) =>onChangeText(val)}
          />
        )}
      </InsetShadow>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 10,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
    marginBottom: hp("2%"),
  },
  tag: {
    height: "100%",
    width: "100%",
    justifyContent: "center",
    paddingHorizontal: 10,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  text: {
    fontSize: rf(12),
  },
  textInput: {
    width: "100%",
    height: "100%",
    paddingHorizontal: 13,
  },
});

export default Categories;
