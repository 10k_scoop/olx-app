import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  AntDesign,
  Feather,
  FontAwesome,
  SimpleLineIcons,
} from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Category from "../../../components/Category";
export default function Header({
  title,
  onBackPress,
}) {
  return (
    <View style={styles.cont1}>
      <TouchableOpacity style={styles.arrow} onPress={onBackPress}>
        <AntDesign name="arrowleft" size={rf(20)} color="white" />
      </TouchableOpacity>
      {/* Search bar */}
      <View style={styles.cont2}>
        <Category name={title} />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    flexDirection: "row",
    alignItems: "center",
    height: hp("10%"),
    justifyContent: "flex-start",
    paddingHorizontal: wp('5%'),
  },
  cont2: {
    width: wp("65%"),
    height: hp("7%"),
    overflow: "hidden",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginLeft:10
  },
  arrow: {
    width: wp("10%"),
    height: hp("5%"),
    borderRadius: 10,
    backgroundColor: "#2ECC71",
    alignItems: "center",
    justifyContent: "center",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
});
