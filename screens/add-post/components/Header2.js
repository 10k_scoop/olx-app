import React from "react";
import { StatusBar, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { Ionicons } from "@expo/vector-icons";
import { RFValue } from "react-native-responsive-fontsize";
export default function Header2(props) {
  return (
    <View style={styles.container}>
      <View style={styles.textWrapper}>
        <Text style={styles.text}>{props.title}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    height: hp("13%"),
    borderBottomWidth: 1,
    width: wp("100%"),
    paddingTop: StatusBar.currentHeight,
    borderColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    flexDirection: "row",
  },
  backBtn:{
      alignItems: "center",
      justifyContent:"center",
      flex:0,
      left:wp('4%')
  },
  textWrapper:{
      flex:1,
      alignItems: "center",
      justifyContent:"center",
  },
  text:{
      fontSize:RFValue(16),
      fontWeight:"700",
      letterSpacing:1
  }
});
