import React, { useEffect, useState } from "react";
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
  StatusBar,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header2 from "./Header2";

const Category = (props) => {
  var barColors = ["lightgreen", "orange", "pink", "purple", "skyblue"];
  const data = props.categories;
  // data.map((item,index) => {
  //   console.log(item)
  // })
  return (
    <View style={styles.container}>
      <Header2 title={"Categories"} />

      <View style={styles.listWrapper}>
        <ScrollView>
          {data.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles.cont}
                key={index}
                onPress={() =>props.onSelect(item.name)}
              >
                <View
                  style={[
                    styles.bar,
                    {
                      backgroundColor: barColors[Math.floor(Math.random() * 5)],
                    },
                  ]}
                ></View>
                <Text style={styles.lisItem}>{item.name}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

export default Category;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    height: hp("100%") + StatusBar.currentHeight,
    width: wp("100%"),
    backgroundColor: "#e5e5e5",
    position: "absolute",
    zIndex: 99999999,
  },
  listWrapper: {
    flex: 1,
    width: hp("100%"),
    alignItems: "center",
    paddingTop: hp("1%"),
  },
  lisItem: {
    fontWeight: "bold",
    fontSize: rf(16),
    marginLeft: 10,
  },
  cont: {
    width: wp("95%"),
    height: hp("7%"),
    backgroundColor: "#fff",
    marginBottom: hp("1.5%"),
    alignItems: "center",
    flexDirection: "row",
  },
  bar: {
    height: "100%",
    width: "1.2%",
  },
});
