import React, { useState, useEffect } from "react";
import { Entypo } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import {
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
  View,
  Image,
  ActivityIndicator,
} from "react-native";
import InsetShadow from "react-native-inset-shadow";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Categories from "./components/Categories";
import Header from "./components/Header";
import * as ImagePicker from "expo-image-picker";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";
import {
  getCategories,
  addPost,
} from "../../state-management/actions/products/actions";
import * as firebase from "firebase";
import Category from "./components/Category";
import SubCategory from "./components/SubCategory";
import * as Location from "expo-location";

const AddPost = (props) => {
  const [image, setImage] = useState(null);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [selectedSubCategory, setSelectedSubCategory] = useState(null);
  const [title, setTitle] = useState(null);
  const [description, setDescription] = useState(null);
  const [price, setPrice] = useState(null);
  const [lang, setLanguage] = useState();
  const [showCategory, setShowCategory] = useState(false);
  const [showSubCategory, setShowSubCategory] = useState(false);
  const user = firebase.auth().currentUser;
  const [loading, setLoading] = useState(true);
  const [fetchedCategories, setFetchedCategories] = useState([]);
  const [fetchedSubCategories, setFetchedSubCategories] = useState();
  const [location, setLocation] = useState(null);
  const [errorMsg, setErrorMsg] = useState(null);

  const db = firebase.firestore();

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        setErrorMsg("Permission to access location was denied");
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  useEffect(() => {
    props?.getLang(setLanguage);
    props?.getCategories(db, setLoading);
  }, []);

  useEffect(() => {
    setFetchedCategories(props.get_categories);
  }, [props]);

  useEffect(() => {
    (async () => {
      if (Platform.OS !== "web") {
        const { status } =
          await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== "granted") {
          alert("Sorry, we need camera roll permissions to make this work!");
        }
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.IMAGE,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setImage(result.uri);
    }
  };

  const Gradient = () => {
    return (
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
    );
  };

  const onSelectCategory = (val) => {
    setSelectedCategory(val);
    setLoading(true);
    fetchedCategories.map((item, index) => {
      if (val == item.name) {
        setFetchedSubCategories(JSON.parse(item.subCategory));
        setShowCategory(false);
        setLoading(false);
      }
    });
  };

  const onSelectSubCategory = (val) => {
    setSelectedSubCategory(val);
    setShowSubCategory(false);
  };

  const onAddPost = () => {
    if (
      !selectedCategory ||
      !selectedSubCategory ||
      title == "" ||
      price == "" ||
      description == ""
    ) {
      alert("fill all details");
    } else {
      setLoading(true);
      var data = {
        addOwner: user.providerData[0].email,
        category: selectedCategory,
        currency: "$",
        uri: image,
        location: location?location:"",
        peopleReached: 0,
        price: price,
        status: true,
        subCategory: selectedSubCategory,
        title: title,
        desc: description,
      };
      props.addPost(db, setLoading, data);
    }
  };
  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      {showCategory && (
        <Category
          categories={fetchedCategories}
          onSelect={(val) => onSelectCategory(val)}
        />
      )}
      {showSubCategory && (
        <SubCategory
          subcategories={fetchedSubCategories}
          onSelect={(val) => onSelectSubCategory(val)}
          selected={selectedSubCategory}
        />
      )}

      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
        start={[0.5, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={{
          position: "absolute",
          width: "100%",
          height:
            Dimensions.get("window").height + +StatusBar.currentHeight + 10,
        }}
      />
      <Header
        title={Strings("addPost", lang)}
        onBackPress={() => props.navigation.goBack()}
      />
      <ScrollView>
        <View style={[styles.addPhoto, , styles.shadow]} onPress={pickImage}>
          <TouchableOpacity onPress={pickImage}>
            <InsetShadow>
              <Gradient />
              {image ? (
                <Image
                  source={{ uri: image }}
                  style={{ width: "100%", height: "100%" }}
                  resizeMode="stretch"
                />
              ) : (
                <View style={styles.inner}>
                  <Entypo
                    name="camera"
                    size={rf(30)}
                    color="#2ECC71"
                    style={{ marginBottom: "3%" }}
                  />
                  <Text style={styles.txt}>{Strings("addPhoto", lang)}</Text>
                </View>
              )}
            </InsetShadow>
          </TouchableOpacity>
        </View>
        <View style={styles.details}>
          <Categories
            name={
              selectedCategory ? selectedCategory : Strings("Category", lang)
            }
            select
            lang={lang}
            selectPress={() => setShowCategory(true)}
          />
          <Categories
            name={
              selectedSubCategory
                ? selectedSubCategory
                : Strings("subCategory", lang)
            }
            select
            lang={lang}
            selectPress={() =>
              selectedCategory
                ? setShowSubCategory(true)
                : alert("Select category first !")
            }
          />
          <Categories
            type="title"
            name={Strings("Title", lang)}
            input
            lang={lang}
            onChangeText={(val) => setTitle(val)}
          />
          <Categories
            type="addDescription"
            name={Strings("addDescription", lang)}
            input
            lang={lang}
            onChangeText={(val) => setDescription(val)}
          />
          <Categories
            name={Strings("Price", lang)}
            onChangeText={(val) => setPrice(val)}
            input
            lang={lang}
          />
          <Categories name={Strings("Location", lang)} select lang={lang} />
        </View>
        <View style={[styles.button, styles.shadow]}>
          <InsetShadow>
            <View style={[styles.inner, { backgroundColor: "#2ECC71" }]}>
              <TouchableOpacity style={styles.inner} onPress={onAddPost}>
                <Text style={[styles.txt, { color: "#fff" }]}>
                  {Strings("addYourPost", lang)}
                </Text>
              </TouchableOpacity>
            </View>
          </InsetShadow>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight + 10,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  adSpace: {
    width: wp("96%"),
    height: hp("20%"),
    marginTop: hp("1%"),
    alignSelf: "center",
    borderRadius: 10,
    overflow: "hidden",
  },
  addPhoto: {
    width: wp("55%"),
    height: hp("15%"),
    marginTop: hp("2%"),
    marginBottom: hp("2%"),
    alignSelf: "center",
    borderRadius: 10,
    overflow: "hidden",
  },
  inner: {
    height: "100%",
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
  txt: {
    fontSize: rf(14),
  },
  details: {
    paddingHorizontal: wp("5%"),
    width: wp("100%"),
    marginTop: 10,
  },
  button: {
    height: hp("6%"),
    width: wp("35%"),
    alignSelf: "center",
    marginBottom: 10,
    borderRadius: 10,
    overflow: "hidden",
  },
  shadow: {
    borderColor: "#fff",
    borderWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
  get_categories: state.main.get_categories,
  add_post: state.main.add_post,
});
export default connect(mapStateToProps, {
  getLang,
  getCategories,
  addPost,
})(AddPost);
