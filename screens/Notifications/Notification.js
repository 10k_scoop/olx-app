import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  StatusBar,
  ScrollView,
  Text,
  View,
} from "react-native";
import Header1 from "../../components/Header1";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Ionicons } from "@expo/vector-icons";
import { LinearGradient } from "expo-linear-gradient";
import NotificationCard from "./components/NotificationCard";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Notification = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <Header1
        screen="profile"
        searchPlaceholder={Strings("searchStore",lang)}
        onBackPress={() => props.navigation.goBack()}
        onSearchPress={() => props.navigation.navigate("Search")}
        onSettingPress={() => props.navigation.navigate("Settings")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.title}>
          <View style={styles.Innertitle}>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.0, 0.5]}
              end={[1.0, 0.5]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <Ionicons name="notifications" size={rf(13)} color="#2ECC71" />
            <Text
              style={{ fontSize: rf(14), marginLeft: 5, fontWeight: "700" }}
            >
              {Strings("Notifications",lang)}
            </Text>
          </View>
        </View>

        <View style={styles.notificationWrapper}>
          <NotificationCard notiText={Strings("demoNoti",lang)} />
          <NotificationCard notiText={Strings("demoNoti",lang)} />
          <NotificationCard notiText={Strings("demoNoti",lang)} />
          <NotificationCard notiText={Strings("demoNoti",lang)} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    paddingBottom: 10,
    backgroundColor: "#fff",
  },
  title: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
    paddingHorizontal: wp("5%"),
  },
  Innertitle: {
    width: "38%",
    height: "48%",
    borderRadius: 10,
    flexDirection: "row",
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  notificationWrapper: {
    width: "100%",
    alignItems: "center",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Notification);
