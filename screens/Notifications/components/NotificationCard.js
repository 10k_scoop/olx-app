import React from "react";
import { StyleSheet, TouchableOpacity, View, Text, ImageBackground } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function NotificationCard(props) {
    return (
        <TouchableOpacity style={styles.cont1}>
                <LinearGradient
                    // Background Linear Gradient
                    colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                    start={[0.0, 0.5]}
                    end={[1.0, 0.5]}
                    locations={[0.2, 1, 1]}
                    style={styles.background}
                />
                <View style={styles.Inner}>
                    
                    <View style={styles.Img}>
                        <ImageBackground
                            style={{ width: "100%", height: "100%" }}
                            source={require("../../../assets/gallery.png")}
                            resizeMode="cover"
                        >
                        </ImageBackground>
                    </View>
                    <View style={styles.Discription}>
                        <Text style={{fontSize:rf(16),fontWeight:"700"}}>iphone 11</Text>
                        <Text style={{fontSize:rf(11),color:"#222"}}>{props.notiText}</Text>
                    </View>
                </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    cont1: {
        width: wp("90%"),
        height: hp("10%"),
        overflow: "hidden",
        borderRadius: 17,
        alignItems: "center",
        flexDirection: "row",
        marginBottom:"4%",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 7,

    },
    background: {
        position: "absolute",
        width: "100%",
        height: "100%",
    },
    Inner: {
        width: wp('90%'),
        height: hp('12%'),
        overflow: "hidden",
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: "6%",
     

    },
    Img: {
        width: hp('8%'),
        height: hp('8%'),
        borderRadius: 100,
        overflow: "hidden"

    },
    Discription: {
        width: "80%",
        height: "60%",
        paddingHorizontal:"8%",
        justifyContent:"space-evenly"
    }

});
