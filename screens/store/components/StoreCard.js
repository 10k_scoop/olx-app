import React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Text,
  ImageBackground,
  Image,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import { AntDesign } from "@expo/vector-icons";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function StoreCard(props) {
  return (
    <TouchableOpacity style={styles.cont1} onPress={props.onPress}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.Inner}>
          <View style={styles.FirstRow}>
            <View style={styles.avatarWrapper}>
              <View style={styles.avatarInner}>
                <Image
                  source={require("../../../assets/user.png")}
                  resizeMode="cover"
                  style={styles.img}
                />
              </View>
            </View>
            <ImageBackground
              style={{ width: "100%", height: "100%", borderRadius: 10 }}
              source={require("../../../assets/StorePic.jpg")}
              resizeMode="cover"
            ></ImageBackground>
          </View>
          <View style={styles.SecondRow}>
            <View style={styles.StoreName}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <Text style={{ fontSize: rf(18), fontWeight: "700" }}>
                Store Name
              </Text>
            </View>
            <View style={styles.Product}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <Text style={{ fontSize: rf(11) }}>100 {props.products}</Text>
            </View>
          </View>
          <View style={styles.ThirdRow}>
            <View style={styles.Category}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <Text style={{ fontSize: rf(11) }}>{props.location}</Text>
            </View>
          </View>
          <View style={styles.LastRow}>
            <View style={styles.Category}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <Text style={{ fontSize: rf(11) }}>{props.category}</Text>
            </View>
            <View style={styles.Rating}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-around",
                  width: "80%",
                }}
              >
                <AntDesign name="star" size={rf(14)} color="#2ECC71" />
                <AntDesign name="star" size={rf(14)} color="#2ECC71" />
                <AntDesign name="star" size={rf(14)} color="#2ECC71" />
                <AntDesign name="star" size={rf(14)} color="#2ECC71" />
                <AntDesign name="star" size={rf(14)} color="#222" />
              </View>
            </View>
          </View>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("92%"),
    height: hp("37%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginHorizontal: wp("2%"),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 7,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  Inner: {
    textAlign: "center",
    fontSize: rf(13),
    width: wp("92%"),
    height: hp("37%"),
    textAlignVertical: "center",
    overflow: "hidden",
  },
  FirstRow: {
    width: "100%",
    height: "52%",
    borderRadius: 10,
    overflow: "hidden",
    alignItems:'center',
    justifyContent:'center'
  },
  SecondRow: {
    width: "100%",
    height: "18%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  StoreName: {
    width: "60%",
    height: "60%",
    alignItems: "center",
    borderWidth: 0.8,
    borderColor: "#fff",
    justifyContent: "center",
    borderRadius: 18,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  Product: {
    width: "28%",
    height: "50%",
    borderRadius: 8,
    borderWidth: 0.8,
    borderColor: "#fff",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  ThirdRow: {
    width: "100%",
    height: "13%",
    justifyContent: "center",
    paddingHorizontal: 13,
  },
  LastRow: {
    width: "100%",
    height: "13%",
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    paddingHorizontal: 12,
  },
  Category: {
    width: "25%",
    height: "80%",
    borderRadius: 8,
    borderWidth: 0.8,
    borderColor: "#fff",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  Rating: {
    width: "35%",
    height: "90%",
    borderRadius: 12,
    overflow: "hidden",
    alignItems: "center",
    borderWidth: 0.8,
    borderColor: "#fff",
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  avatarInner: {
    height: hp("9%"),
    width: hp("9%"),
    alignItems: "center",
    justifyContent: "center",
  },
  img: {
    height: "80%",
    width: "80%",
  },
  avatarWrapper: {
    height: hp("9%"),
    width: hp("9%"),
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff",
    position: "absolute",
    borderRadius: 100,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 7,
  },
});
