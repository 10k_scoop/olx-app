import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  SafeAreaView,
  StatusBar,
  ScrollView,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import FilterBtn from "../../components/FilterBtn";
import StoreCard from "./components/StoreCard";
import Header1 from "../../components/Header1";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const Store = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <LinearGradient
        colors={["#FFFFFF", "#E3EDF7", "#E3EDF7"]}
        start={[0.1, 0.0]}
        end={[0.5, 1.0]}
        locations={[0.2, 1, 1]}
        style={styles.background}
      />
      <Header1
        screen={"Store"}
        searchPlaceholder={Strings("searchStore", lang)}
        onBackPress={() => props.navigation.goBack()}
        onSearchPress={() => props.navigation.navigate("Search")}
        onBellPress={() => props.navigation.navigate("Notification")}
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.menu}>
          <FilterBtn txt={Strings("Store", lang)} />
          <FilterBtn txt={Strings("CAS", lang)} />
          <FilterBtn txt={Strings("Filter", lang)} filter />
        </View>

        <View style={styles.cardsWrapper}>
          <StoreCard
            products={Strings("products", lang)}
            location={Strings("Location", lang)}
            category={Strings("Category", lang)}
          />
          <StoreCard
            products={Strings("products", lang)}
            location={Strings("Location", lang)}
            category={Strings("Category", lang)}
          />
          <StoreCard
            products={Strings("products", lang)}
            location={Strings("Location", lang)}
            category={Strings("Category", lang)}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: StatusBar.currentHeight,
    alignItems: "center",
  },
  menu: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: hp("100%") + StatusBar.currentHeight,
  },
  cardsWrapper: {
    alignItems: "center",
  },
});
const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Store);
