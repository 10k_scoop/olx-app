import React, { useEffect, useState } from "react";
import {
  TouchableOpacity,
  StyleSheet,
  View,
  Text,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import Header from "./components/Header";
import * as firebase from "firebase";
import { getSubCategory } from "../../state-management/actions/products/actions";
import { connect } from "react-redux";

const SubCategory = (props) => {

  var barColors = ["lightgreen", "orange", "pink", "purple", "skyblue"];
  const [loading, setLoading] = useState(true);
  const db = firebase.firestore();
  const [categories, setCategories] = useState();

  useEffect(() => {
    props?.getSubCategory(db, setLoading, props.route.params.title,setCategories);
  }, []);

  // useEffect(() => {
  //   setCategories(JSON.parse(props?.get_subcategory[0]?.subCategory));
  // }, []);

  if (loading) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#222" />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Header
        title={props.route.params.title}
        goBack={() => props.navigation.goBack()}
      />

      <View style={styles.listWrapper}>
        <ScrollView>
          {categories?.map((item, index) => {
            return (
              <TouchableOpacity
                style={styles.cont}
                key={index}
                onPress={() =>
                  props.navigation.navigate("NearAds", {
                    category: props.route.params.title,
                    subCategory: item,
                  })
                }
              >
                <View
                  style={[
                    styles.bar,
                    {
                      backgroundColor: barColors[Math.floor(Math.random() * 5)],
                    },
                  ]}
                ></View>
                <Text style={styles.lisItem}>{item}</Text>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
};

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_subcategory: state.main.get_subcategory,
});
export default connect(mapStateToProps, { getSubCategory })(SubCategory);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#e5e5e5",
  },
  listWrapper: {
    flex: 1,
    width: hp("100%"),
    alignItems: "center",
    paddingTop: hp("1%"),
  },
  lisItem: {
    fontWeight: "bold",
    fontSize: rf(16),
    marginLeft: 10,
  },
  cont: {
    width: wp("95%"),
    height: hp("7%"),
    backgroundColor: "#fff",
    marginBottom: hp("1.5%"),
    alignItems: "center",
    flexDirection: "row",
  },
  bar: {
    height: "100%",
    width: "1.2%",
  },
});
