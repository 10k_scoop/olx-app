import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  SafeAreaView,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { LinearGradient } from "expo-linear-gradient";
import { Ionicons } from "@expo/vector-icons";
import CreditCard from "./components/CreditCard";
import InsetShadow from "react-native-inset-shadow";
import Strings from "../../constants/lng/LocalizationStrings";
import { getLang } from "../../state-management/actions/lang/Main";
import { connect } from "react-redux";

const BuyCredit = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.cont1}>
        <View style={styles.curve}>
          <View style={styles.layer}></View>
          <View style={styles.BackBtnRow}>
            <TouchableOpacity
              style={styles.backbtn}
              onPress={() => props.navigation.goBack()}
            >
              <Ionicons name="arrow-back" size={24} color="#fff" />
            </TouchableOpacity>
          </View>

          <View style={styles.walletBtnRow}>
            <View style={styles.walletBtn}>
              <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
              />
              <Text style={{ fontSize: rf(14), fontWeight: "700" }}>
                {Strings("Wallet", lang)}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.Dot1}></View>
        <View style={styles.Dot2}></View>
        <View style={styles.Dot3}></View>
        <View style={styles.Dot4}></View>
      </View>

      <View style={styles.walletBox}>
        <InsetShadow>
          <LinearGradient
            // Background Linear Gradient
            colors={["#2ECC71", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <View style={styles.InnerWallet}>
            <Text
              style={{ fontSize: rf(14), color: "#fff", fontWeight: "700" }}
            >
              {Strings("Credit", lang)}
            </Text>
          </View>
          <View style={styles.InnerSecondRow}>
            <Text
              style={{ fontSize: rf(24), color: "#fff", fontWeight: "700" }}
            >
              $60
            </Text>
          </View>
        </InsetShadow>
      </View>
      <View style={styles.purchaseCredit}>
        <View style={styles.InnerPurchase}>
          <LinearGradient
            // Background Linear Gradient
            colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
            start={[0.0, 0.5]}
            end={[1.0, 0.5]}
            locations={[0.2, 1, 1]}
            style={styles.background}
          />
          <Text style={{ fontSize: rf(12), fontWeight: "700" }}>
            {Strings("purchaseCredit", lang)}
          </Text>
        </View>
      </View>
      <View style={styles.CardsRow}>
        <CreditCard text={Strings("Credit", lang)} txt1="10" txt2="$25" />
        <CreditCard txt1="5" txt2="$15" text={Strings("Credit", lang)} />
      </View>
      <View style={styles.CardsRow}>
        <CreditCard txt1="30" txt2="$2" text={Strings("Credit", lang)} />
        <CreditCard txt1="60" txt2="$30" text={Strings("Credit", lang)} />
      </View>
      <View style={styles.CardsLastRow}>
        <CreditCard txt1="10" txt2="$8" text={Strings("Credit", lang)} />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#fff",
  },
  cont1: {
    width: wp("100%"),
    height: hp("28%"),
  },
  curve: {
    width: "80%",
    height: "98%",
  },
  layer: {
    width: "80%",
    height: "98%",
    backgroundColor: "#2ECC71",
    borderBottomRightRadius: hp("100%"),
    position: "absolute",
    borderBottomLeftRadius: 200,
    transform: [{ scaleX: 1.4 }],
  },
  walletBtn: {
    width: "55%",
    height: hp("4.5"),
    backgroundColor: "#fff",
    marginLeft: "20%",
    borderRadius: 8,
    borderWidth: 2,
    overflow: "hidden",
    borderColor: "#e5e5e5",
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  backbtn: {
    width: "13%",
    height: "30%",
    borderRadius: 8,
    backgroundColor: "#149E4E",
    marginLeft: 12,
    justifyContent: "center",
    alignItems: "center",
  },
  Dot1: {
    width: hp("3.2%"),
    height: hp("3.2%"),
    borderRadius: 100,
    backgroundColor: "#2ECC71",
    right: 35,
    top: 70,
    position: "absolute",
  },
  Dot2: {
    width: hp("1.5%"),
    height: hp("1.5%"),
    borderRadius: 100,
    backgroundColor: "#2ECC71",
    right: 54,
    top: 100,
    position: "absolute",
  },
  Dot3: {
    width: hp("1%"),
    height: hp("1%"),
    borderRadius: 100,
    backgroundColor: "#2ECC71",
    right: 14,
    top: 70,
    position: "absolute",
  },
  Dot4: {
    width: hp("1%"),
    height: hp("1%"),
    borderRadius: 100,
    backgroundColor: "#2ECC71",
    right: 22,
    top: 60,
    position: "absolute",
  },
  walletBox: {
    width: "90%",
    height: "13%",
    borderRadius: 10,
    backgroundColor: "#2ECC71",
    borderWidth: 0.6,
    borderColor: "#fff",
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 9.75,
    shadowRadius: 3.84,

    elevation: 17,
  },
  purchaseCredit: {
    width: "90%",
    height: "8%",
    justifyContent: "center",
  },
  InnerPurchase: {
    width: "32%",
    height: "40%",
    borderRadius: 8,
    justifyContent: "center",
    overflow: "hidden",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  CardsRow: {
    width: "90%",
    height: "15%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    marginBottom: 5,
  },
  CardsLastRow: {
    width: "90%",
    height: "15%",
    alignItems: "center",
    marginTop: 10,
  },
  InnerWallet: {
    width: "100%",
    height: "35%",
    justifyContent: "flex-end",
    paddingHorizontal: "8%",
  },
  InnerSecondRow: {
    width: "100%",
    height: "65%",
    alignItems: "center",
  },
  BackBtnRow: {
    width: "80%",
    height: "50%",
    justifyContent: "flex-end",
  },
  walletBtnRow: {
    width: "50%",
    height: "50%",
    justifyContent: "center",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(BuyCredit);
