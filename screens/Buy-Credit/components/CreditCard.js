import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
    heightPercentageToDP as hp,
    widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";

export default function CreditCard({txt1,txt2, text }) {
    return (
        <TouchableOpacity style={styles.cont1}>
            <LinearGradient
                // Background Linear Gradient
                colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
                start={[0.0, 0.5]}
                end={[1.0, 0.5]}
                locations={[0.2, 1, 1]}
                style={styles.background}
            />
            <View style={{ alignItems: "center", marginBottom: 10,justifyContent:'center' }}>
                <Text style={{ color: "#222", fontSize: rf(14),fontWeight:'700' }}>{txt1}</Text>
                <Text style={{ color: "#222", fontSize: rf(14),fontWeight:'bold' }}>{text}</Text>
            </View>
            <View style={styles.Btn}>
                <Text style={{ color: "#fff", fontSize: rf(12), fontWeight: "bold" }}>{txt2}</Text>
            </View>

        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    cont1: {
        width: wp("25%"),
        height: hp("13%"),
        overflow: "hidden",
        borderRadius: 17,
        alignItems: "center",
        justifyContent: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 10,

    },
    background: {
        position: "absolute",
        width: "100%",
        height: "100%",
    },
    Btn: {
        width: "70%",
        height: "27%",
        borderRadius: 10,
        backgroundColor: "#2ECC71",
        justifyContent: "center",
        alignItems: "center"
    }

});
