import React, { useRef, useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  StatusBar,
  Image,
  TouchableOpacity,
} from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import BgLayer from "./components/BgLayer";
import SocialLoginBtn from "./components/SocialLoginBtn";
import Strings from "../../constants/lng/LocalizationStrings";
import { connect } from "react-redux";
import { setLang, getLang } from "../../state-management/actions/lang/Main";
import { socialLoginFacebook } from "../../state-management/actions/auth/authActions";

const LoginScreen = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
    console.log(props.route);
  }, []);

  const changeLanguage = () => {
    props?.setLang(lang, setLanguage);
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={{ zIndex: -1, paddingTop: StatusBar.currentHeight }}>
        <BgLayer />
        <TouchableOpacity style={styles.langContOuter} onPress={changeLanguage}>
          <View style={styles.langCont}>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.0, 0.5]}
              end={[1.0, 0.5]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <Text style={{ fontSize: 15, fontWeight: "700", letterSpacing: 1 }}>
              English /عربى
            </Text>
          </View>
        </TouchableOpacity>
        {/* logo */}
        <View style={styles.logoWrapper}>
          <Image
            source={require("../../assets/logo.png")}
            resizeMode="contain"
            style={{ width: "100%", height: "100%" }}
          />
        </View>
        <View style={styles.socialWrapper}>
          <SocialLoginBtn
            iconColor="#2E87CC"
            iconName="facebook"
            title={Strings("LWF", lang)}
            onPress={() => props.socialLoginFacebook()}
            type="facebook"
          />
          <SocialLoginBtn
            iconColor="#2E87CC"
            iconName="google"
            title={Strings("LWG", lang)}
            type="google"
          />
          <SocialLoginBtn
            iconColor="#D0EF40"
            iconName="snapchat"
            title={Strings("LWS", lang)}
            type="snapchat"
          />
          <SocialLoginBtn
            iconColor="#D0EF40"
            iconName="snapchat"
            title={Strings("LWP", lang)}
            type="phone"
            navigation={props.navigation}
          />
        </View>
        <View style={styles.langContOuter}>
          <TouchableOpacity style={styles.skipCont} onPress={()=>props.navigation.navigate("ProductHome")}>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.0, 0.5]}
              end={[1.0, 0.5]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <Text style={{ fontSize: 15, fontWeight: "700", left: 5 }}>
              {Strings("Skip", lang)}
            </Text>
            <View style={{ left: 3 }}>
              <Image
                source={require("../../assets/socialIcons/skipIcon.png")}
                resizeMode="center"
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  langCont: {
    width: wp("26%"),
    height: hp("3%"),
    backgroundColor: "#fff",
    borderRadius: 5,
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("5%"),
  },
  skipCont: {
    width: wp("18.5%"),
    height: hp("3.5%"),
    backgroundColor: "#fff",
    borderRadius: 5,
    overflow: "hidden",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp("9%"),
    flexDirection: "row",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  langContOuter: {
    alignItems: "flex-end",
    paddingHorizontal: wp("5%"),
  },
  logoWrapper: {
    width: wp("100%"),
    height: hp("30%"),
    marginTop: hp("13%"),
  },
  socialWrapper: {
    alignItems: "center",
    justifyContent: "space-evenly",
    height: hp("35%"),
    bottom: hp("3%"),
  },

});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  set_Lang: state.main.set_Lang,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, {
  setLang,
  getLang,
  socialLoginFacebook,
})(LoginScreen);
