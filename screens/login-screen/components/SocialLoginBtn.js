import React, { useRef, useState } from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import { FontAwesome, MaterialCommunityIcons } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import CountryPicker from "react-native-country-picker-modal";
import { CountryCode, Country } from "./types";
export default function SocialLoginBtn({
  iconName,
  iconColor,
  title,
  onPress,
  type,
  navigation,
}) {
  const [countryCode, setCountryCode] = useState("US");
  const [country, setCountry] = useState();
  const [withFlag, setWithFlag] = useState(true);
  const [withCallingCode, setWithCallingCode] = useState(false);
  const [withFilter, setWithFilter] = useState(true);

  const onSelect = (country) => {
    setCountryCode(country.cca2);
    setCountry(country);
    console.log(country);
    console.log(country.callingCode);
  };
  return (
    <TouchableOpacity
      style={styles.cont1}
      onPress={()=>type=="phone"?navigation.navigate("PhoneVerification", { country: country,code:countryCode }):onPress()}
    >
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />

        <View style={styles.socialWrapper}>
          <View style={styles.icon}>
            <InsetShadow>
              {type == "phone" ? (
                <View
                  style={{
                    paddingLeft: wp("0.7%"),
                  }}
                >
                  <CountryPicker
                    {...{
                      countryCode,
                      withFlag,
                      withCallingCode,
                      onSelect,
                      withFilter,
                    }}
                    visible={false}
                    containerButtonStyle={{
                      height: "100%",
                      width: "100%",
                      justifyContent: "center",
                    }}
                  />
                </View>
              ) : (
                <View
                  style={{
                    alignItems: "center",
                    justifyContent: "center",
                    flex: 1,
                  }}
                >
                  {iconName == "google" ? (
                    <Image
                      source={require("../../../assets/socialIcons/google.png")}
                      resizeMode="center"
                    />
                  ) : iconName == "snapchat" ? (
                    <Image
                      source={require("../../../assets/socialIcons/snapchat.png")}
                      resizeMode="center"
                    />
                  ) : (
                    <FontAwesome
                      name={iconName}
                      size={rf(20)}
                      color={iconColor}
                    />
                  )}
                </View>
              )}
            </InsetShadow>
          </View>
          <View style={styles.title}>
            <Text style={{ fontSize: rf(14), fontWeight: "600" }}>{title}</Text>
          </View>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("70%"),
    height: hp("7%"),
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    overflow: "hidden",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  socialWrapper: {
    flex: 1,
    alignItems: "center",
    width: wp("70%"),
    height: hp("7%"),
    paddingHorizontal: "5%",
    flexDirection: "row",
  },
  icon: {
    width: wp("9%"),
    height: hp("4%"),
    backgroundColor: "white",
    borderRadius: 5,
    overflow: "hidden",
  },
  title: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
});
