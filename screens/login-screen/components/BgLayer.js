import React from "react";
import {StyleSheet, View } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";

export default function BgLayer() {
  return (
    <View style={styles.cont1}>
      <View style={styles.cont2}>
        <View style={styles.cont3}>
          <View style={styles.cont4}>
            <View style={styles.cont5}></View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    height: hp("92%"),
    backgroundColor: "#2ECC71",
    borderBottomRightRadius: hp("30%"),
    borderBottomLeftRadius: hp("30%"),
    transform: [{ scaleX: 1.3 }],
    position: "absolute",
  },
  cont2: {
    width: wp("100%"),
    height: hp("83%"),
    backgroundColor: "#78E1B1",
    borderBottomRightRadius: hp("20%"),
    borderBottomLeftRadius: hp("50%"),
    transform: [{ scaleX: 1.1 }],
    zIndex: 122,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
  cont3: {
    width: wp("100%"),
    height: "100%",
    backgroundColor: "#64D499",
    borderBottomRightRadius: hp("50%"),
    overflow: "hidden",
    transform: [{ scaleY: 1.5 }],
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 1,
  },
  cont4: {
    width: wp("100%"),
    height: "86%",
    backgroundColor: "#71D6A3",
    borderBottomRightRadius: hp("80%"),
    overflow: "hidden",

    transform: [{ scaleY: 1 }],
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 1,
  },
  cont5: {
    width: wp("100%"),
    height: "70%",
    backgroundColor: "#7DD9AB",
    borderBottomRightRadius: hp("100%"),
    transform: [{ scaleY: 2 }],
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 1,
  },
});
