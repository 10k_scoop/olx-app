export default {
  LWF: "Login using facebook",
  LWG: "Login using google",
  LWS: "Login using snapchat",
  Signup: "Signup",
  Skip: "Skip",
  Myads: "My ads",
  Nearads: "Near ads",
  Store: "Store",
  Category: "Category",
  Home: "Home",
  Chat: "Chat",
  Video: "Video",
  Profile: "Profile",
  addPost: "Add Post",
  Search: "Search",
  Categories: "Categories",
  recentSearch: "Recent Search",
  seeMore: "See more",
  Suggestion: "Suggestion",
  Active: "Active",
  Expired: "Expired",
  Edit: "Edit",
  Delete: "Delete",
  Update: "Update",
  Share: "Share",
  Boost: "Boost",
  Status: "Status",
  People: "People",
  Saw: "Saw",
  Yesterday: "Yesterday",
  Filter: "Filter",
  min: "min ago",
  kilometer: "kilometer",
  Title: "Title",
  username: "username",
  ads: "ads",
  searchStore: "Search Store",
  CAS: "Create a store",
  products: "products",
  Location: "Location",
  AllAds: "All Ads",
  Unread: "Unread",
  Deleted: "Deleted",
  Today: "Today",
  Sneaker: "Sneaker",
  SIYC: "Search in your chat",
  Online: "Online",
  FAYA: "Features and your Ads",
  GT: "General Tips",
  CTL1: "Only meet in public places",
  CTL2: "Never pay and transfer money in advance",
  CTL3: "Inspect the product before you buy it",
  SNM: "Send New Message",
  addPhoto: "Add Photo",
  subCategory: "Sub Category",
  addDescription: "Add Description",
  Price: "Price",
  addYourPost: "Add Your Post",
  Local: "Local",
  Follow: "Follow",
  videoTitle: "Most amazing capture of nature",
  createVideo: "Create Video",
  Name: "Name",
  Followers: "Followers",
  Verified: "Verified",
  myVideoAds: "My Video Ads",
  myOrder: "My Order",
  removePromotionalAds: "Remove Promotional Ads",
  recentViewAds: "Recent View Ads",
  myWallet: "My Wallet",
  showMembership: "Show Membership",
  favoriteAds: "Favorite Ads",
  inviteYourFriend: "Invite Your Friend",
  Help: "Help",
  contactUs: "contact Us",
  Country: "Country",
  darkTheme: "Dark Theme",
  Notifications: "Notifications",
  blockedUsers: "Blocked Users",
  termsOfUse: "Terms Of Use",
  privacyPolicy: "Privacy Policy",
  aboutThisApp: "About This App",
  Riyad: "Riyad",
  Mobile: "Mobile",
  searchM: "Search Mustaemall",
  Settings: "Settings",
  Language: "Language",
  demoNoti: "Lorem Ipsum",
  Information: "information",
  Brand: "Brand",
  Model: "Model",
  bikeMake: "Bike Make",
  Year: "Year",
  Transmission: "Transmission",
  Fuel: "Fuel",
  Color: "Color",
  paymentMethod: "Payment Method",
  postId: "Post ID",
  publishDate: "Publish Date",
  shareTheAd: "Share the Ad",
  postOwner: "Post Owner",
  responseTime: "Response Time",
  memberSince: "Member since",
  Rating: "Rating",
  moreAds: "More Ads",
  Comments: "Comments",
  WYCH: "Write your comment here ..",
  ASPF: "Add same post free",
  Description: "Description",
  dummyText:
    "lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs",
  Hour: "Hour",
  Wallet: "Wallet",
  Credit: "Credit",
  purchaseCredit: "Purchase Credit",
  CAB:"Car and bikes",
  CFS:"Car for sale",
  Honda: "Honda",
  Automatic: "Automatic",
  Gasoline: "Gasoline",
  Black:"Black",
  cashOnly: "Cash Only",
  hoursAgo: "Hours ago",
  AFP: "Ask for price",
  Logout: "Logout",
  LWP:"Login using phone"

};
