import * as Localization from "expo-localization";
import i18n from "i18n-js";
import en from "./en";
import ar from "./ar";

const Strings = (keyword,lan,setLang) => {
  i18n.translations = {
    en: en,
    ar: ar,
  };
  i18n.fallbacks = true;
  // Set the locale once at the beginning of your app.
  i18n.locale = lan?lan:Localization.locale;
  return i18n.t(keyword);
};

export default Strings;
