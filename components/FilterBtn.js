import React from "react";
import { StyleSheet, TouchableOpacity, View, Text } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { FontAwesome, SimpleLineIcons } from "@expo/vector-icons";

export default function FilterBtn({ txt,filter, onPress, textBold, textLarge }) {
  return (
    <TouchableOpacity style={styles.cont1} onPress={onPress}>
      <InsetShadow>
        <LinearGradient
          // Background Linear Gradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.0, 0.5]}
          end={[1.0, 0.5]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.Inner}>
          {filter && (
            <FontAwesome name="filter" size={rf(16)} color="#2ECC71" style={{right:5}} />
          )}
          {txt == "All Ads" && (
            <SimpleLineIcons
              name="equalizer"
              size={rf(16)}
              color="#2ECC71"
              style={{ transform: [{ rotate: "90deg" }] }}
            />
          )}
          <Text
            style={[
              styles.txt,
              {
                marginHorizontal: 0,
                fontWeight: textBold && "bold",
                fontSize: textLarge && rf(14),
              },
            ]}
          >
            {txt}
          </Text>
        </View>
      </InsetShadow>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  cont1: {
    minWidth: wp("26%"),
    height: hp("4.5%"),
    borderRadius: 10,
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 7,
  },
  background: {
    position: "absolute",
    width: "105%",
    height: "100%",
  },
  Inner: {
    flex: 1,
    width: wp("26%"),
    height: hp("5%"),
    alignItems: "center",
    justifyContent: "center",
    flexDirection: "row",
  },
  txt: {
    fontSize: rf(12),
    fontWeight: "600",
  },
});
