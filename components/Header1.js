import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Text,
  TextInput,
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import {
  AntDesign,
  Feather,
  FontAwesome,
  SimpleLineIcons,
} from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Category from "./Category";
import { getLang } from "../state-management/actions/lang/Main";
import { connect } from "react-redux";
import Strings from "../constants/lng/LocalizationStrings";
const Header1 = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  return (
    <View style={styles.cont1}>
      {/* Logo */}
      <View style={styles.logo}>
        {props.screen == "Home" ? (
          <Image
            source={require("../assets/smallLogo.png")}
            resizeMode="contain"
            style={{ width: "100%", height: "100%" }}
          />
        ) : (
          <TouchableOpacity style={styles.bellIcon} onPress={props.onBackPress}>
            <AntDesign name="arrowleft" size={rf(16)} color="white" />
          </TouchableOpacity>
        )}
      </View>
      {/* Search bar */}
      {props.screen == "Ads" ? (
        <View style={styles.cont2}>
          <Category name={Strings('Myads',lang)} />
          <TouchableOpacity style={styles.bellIcon}>
            <SimpleLineIcons
              name="equalizer"
              size={rf(16)}
              color="white"
              style={{ transform: [{ rotate: "90deg" }] }}
            />
          </TouchableOpacity>
        </View>
      ) : (
        <View style={styles.searchBar}>
          <InsetShadow>
            <LinearGradient
              // Background Linear Gradient
              colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
              start={[0.0, 0.5]}
              end={[1.0, 0.5]}
              locations={[0.2, 1, 1]}
              style={styles.background}
            />
            <TouchableOpacity
              style={{
                flex: 1,
                flexDirection: "row",
                alignItems: "center",
                paddingHorizontal: 5,
              }}
              onPress={props.onSearchPress}
            >
              {props.active === "search" ? (
                <TextInput
                  placeholder={props.searchPlaceholder || Strings("searchM",lang)}
                  placeholderTextColor="grey"
                  style={[styles.searchInput,{paddingHorizontal:10}]}
                />
              ) : (
                <View style={styles.searchInput}>
                  <Text style={{ color: "grey" }}>
                    {props.searchPlaceholder || Strings("searchM",lang)}
                  </Text>
                </View>
              )}

              <View style={styles.searchIcon}>
                <FontAwesome name="search" size={rf(16)} color="white" />
              </View>
            </TouchableOpacity>
          </InsetShadow>
        </View>
      )}
      {/* Bell icon */}
      <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
        <TouchableOpacity
          style={styles.bellIcon}
          onPress={
            props.onSettingPress
              ? props.onSettingPress
              : props.onBellPress
              ? props.onBellPress
              : undefined
          }
        >
          {props.screen == "profile" ? (
            <Feather name="settings" size={rf(18)} color="white" />
          ) : props.screen == "search" || props.screen == "category" ? (
            <SimpleLineIcons
              name="equalizer"
              size={rf(18)}
              color="white"
              style={{ transform: [{ rotate: "90deg" }] }}
            />
          ) : (
            <FontAwesome name="bell" size={rf(16)} color="white" />
          )}
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    flexDirection: "row",
    alignItems: "center",
    height: hp("10%"),
    justifyContent: "space-evenly",
    paddingHorizontal: 5,
  },
  logo: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  cont2: {
    width: wp("65%"),
    height: hp("7%"),
    overflow: "hidden",
    marginHorizontal: "5%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  searchBar: {
    width: wp("73%"),
    height: hp("7%"),
    overflow: "hidden",
    borderRadius: 15,
    marginHorizontal: 5,
  },
  searchInput: {
    width: "85%",
    height: "100%",
    paddingLeft: 5,
    fontSize: 14,
    alignItems: "flex-start",
    justifyContent: "center",
  },
  searchIcon: {
    width: "13%",
    height: "60%",
    borderRadius: 5,
    backgroundColor: "#2ECC71",
    alignItems: "center",
    justifyContent: "center",
  },
  bellIcon: {
    width: wp("10%"),
    height: hp("5%"),
    borderRadius: 10,
    backgroundColor: "#2ECC71",
    alignItems: "center",
    justifyContent: "center",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(Header1);
