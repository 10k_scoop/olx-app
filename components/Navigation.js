import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";

const Navigation = ({ active, onSearchPress, onCategoryPress,text }) => {
  return (
    <View style={styles.nav}>
      <TouchableOpacity
        style={[styles.navInner, active == "search" && styles.active]}
        onPress={onSearchPress}
      >
        <Text style={styles.navText}>{text.search}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={[styles.navInner, active == "category" && styles.active]}
        onPress={onCategoryPress}
      >
        <Text style={styles.navText}>{text.categories}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  nav: {
    width: wp("100%"),
    height: hp("6%"),
    flexDirection: "row"
  },
  navInner: {
    width: "50%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center"
  },
  navText: {
    fontSize: rf(15),
    fontWeight: "bold"
  },
  active: {
    borderColor: "transparent",
    borderWidth: 2,
    borderBottomColor: "#2ECC71",
  }
});

export default Navigation;
