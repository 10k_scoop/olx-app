import { LinearGradient } from 'expo-linear-gradient';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import InsetShadow from 'react-native-inset-shadow';
import { RFValue as rf } from 'react-native-responsive-fontsize';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';

const Category = ({name}) => {
  return (
    <View style={styles.container}>
      <InsetShadow>
        <LinearGradient
          colors={["#FFFFFF", "#E3EDF7", "#E5E5E5"]}
          start={[0.5, 0.0]}
          end={[0.5, 1.0]}
          locations={[0.2, 1, 1]}
          style={styles.background}
        />
        <View style={styles.tag}>
          <Text style={styles.text}>{name}</Text>
        </View>
      </InsetShadow>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    overflow: "hidden",
    borderColor: "#fff",
    borderWidth: 1,
    height: hp("5%"),
    width: "40%",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1
    },
    shadowOpacity: 0.1,
    shadowRadius: 20,
    elevation: 7,
    marginHorizontal:5
  },
  tag: {
    alignItems: "center",
    height: "100%",
    width: "100%",
    flexDirection: "row",
    justifyContent:"center"
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%"
  },
  text: {
    fontSize: rf(16),
    fontWeight: "bold",
  }
});

export default Category;
