import React, { useState, useEffect } from "react";
import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from "react-native-responsive-screen";
import { LinearGradient } from "expo-linear-gradient";
import InsetShadow from "react-native-inset-shadow";
import { FontAwesome, FontAwesome5 } from "@expo/vector-icons";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Strings from "../constants/lng/LocalizationStrings";
import { getLang } from "../state-management/actions/lang/Main";
import { connect } from "react-redux";

const BottomMenu = (props) => {
  const [lang, setLanguage] = useState();

  useEffect(() => {
    props?.getLang(setLanguage);
  }, []);

  var activeColor = "#2ECC71";

  return (
    <View style={styles.body}>
      <View style={styles.cont1}>
        <InsetShadow elevation={5}>
          <View style={styles.innerCont}>
            <TouchableOpacity style={styles.item} onPress={props.onHomePress}>
              <FontAwesome
                name="home"
                size={rf(20)}
                color={props.active == "home" ? activeColor : "#D4E0EC"}
              />
              <Text
                style={[
                  styles.text,
                  {
                    color:
                      props.active == "home" ? activeColor : "#D4E0EC",
                  },
                ]}
              >
                {Strings("Home",lang)}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.item} onPress={props.onChatPress}>
              <FontAwesome
                name="send"
                size={rf(18)}
                color={props.active == "chat" ? activeColor : "#D4E0EC"}
              />
              <Text
                style={[
                  styles.text,
                  {
                    color:
                      props.active == "chat" ? activeColor : "#D4E0EC",
                  },
                ]}
              >
                {Strings("Chat",lang)}
              </Text>
            </TouchableOpacity>
            <View style={styles.item}></View>

            <TouchableOpacity style={styles.item} onPress={props.onVideoPress}>
              <FontAwesome
                name="video-camera"
                size={rf(18)}
                color={props.active == "video" ? activeColor : "#D4E0EC"}
              />
              <Text
                style={[
                  styles.text,
                  {
                    color:
                      props.active == "video" ? activeColor : "#D4E0EC",
                  },
                ]}
              >
                {Strings("Video",lang)}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.item}
              onPress={props.onProfilePress}
            >
              <FontAwesome5
                name="user-alt"
                size={rf(18)}
                color={
                  props.active == "profile" ? activeColor : "#D4E0EC"
                }
              />
              <Text
                style={[
                  styles.text,
                  {
                    color:
                      props.active == "profile" ? activeColor : "#D4E0EC",
                  },
                ]}
              >
                {Strings("Profile",lang)}
              </Text>
            </TouchableOpacity>
          </View>
        </InsetShadow>
      </View>
      <TouchableOpacity
        style={styles.middleIcon}
        onPress={props.onAddPostPress}
      >
        <FontAwesome
          name="camera"
          size={rf(20)}
          color="#fff"
          style={{ top: 3 }}
        />
        <Text
          style={{
            color: "#fff",
            fontSize: rf(10),
            fontWeight: "700",
            bottom: 3,
          }}
        >
           {Strings("addPost",lang)}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  cont1: {
    width: wp("100%"),
    alignItems: "center",
    height: hp("7%"),
    justifyContent: "space-evenly",
    paddingHorizontal: 5,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: "#fff",
    overflow: "hidden",
    
  },
  body: {
    width: wp("100%"),
    justifyContent: "flex-end",
    alignItems: "center",
  },
  background: {
    position: "absolute",
    width: "100%",
    height: "100%",
  },
  innerCont: {
    width: wp("100%"),
    alignItems: "center",
    height: hp("8%"),
    flexDirection: "row",
  },
  item: {
    flex: 1,
    alignItems: "center",
    justifyContent: "space-around",
  },
  text: {
    fontSize: rf(11),
    fontWeight: "700",
    marginTop: 3,
  },
  middleIcon: {
    width: wp("17%"),
    height: wp("17%"),
    borderRadius: 100,
    justifyContent: "space-evenly",
    alignItems: "center",
    backgroundColor: "#2ECC71",
    bottom: "40%",
    position: "absolute",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 3,
  },
  circle: {
    width: 1,
    height: 1,
    backgroundColor: "green",
    borderRadius: 100,
    position: "absolute",
    bottom: "20%",
    left: "7%",
    alignItems: "center",
    justifyContent: "flex-end",
    
  },
});

const mapStateToProps = (state) => ({
  errors: state.errors.errors,
  get_Lang: state.main.get_Lang,
});
export default connect(mapStateToProps, { getLang })(BottomMenu);
