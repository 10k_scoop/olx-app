import React, { useState, useEffect } from "react";
import { NoAuthNavigator } from "./routes/NoAuthNavigator";
import { AuthNavigator } from "./routes/AuthNavigator";
import { Provider } from "react-redux";
import store from "./state-management/store";
import * as firebase from "firebase";

export default function App() {
  const [status, setStatus] = useState(false);

  useEffect(() => {

    // Initialize Firebase
    var firebaseConfig = {
      apiKey: "AIzaSyBvidRVJli9icmlvxnfFeMRWsBr6n1NtTU",
      authDomain: "olx-test-4cd15.firebaseapp.com",
      projectId: "olx-test-4cd15",
      storageBucket: "olx-test-4cd15.appspot.com",
      messagingSenderId: "1099146923309",
      appId: "1:1099146923309:web:bf630ddbfd231a997a17a4",
      measurementId: "G-YWLBRXVBY8",
    };
    // Initialize Firebase
    try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(firebaseConfig);
      }
    } catch (error) {
      alert("Check you internet connection!!");
    }

    firebase.auth().onAuthStateChanged(function (user) {
      if (user) {
        setStatus("LoggedIn")
        console.log("User logged in")
      } else {
        setStatus(false);
      }
    });
  }, []);



  return (
    <Provider store={store}>
      {status=="LoggedIn"?<AuthNavigator/>:<NoAuthNavigator />}
    </Provider>
  );
}
